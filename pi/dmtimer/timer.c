/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 4 Laboratory
 *
 * Abstract:    Peripheral devices implementation with C
 *
 * Purpose:     Timer that does the job for the metronomelab
 *
 * Author:      Bompard Corentin & Hoxha Arton
 *
 * Date: 		25.03.2020
 */

#include "timer.h"

#include <bbb/am335x_clock.h>
#include <bbb/am335x_gpio.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "../interrupt/intc.h"

#define IRQ_STATUS (1 << 1)
#define IRQ_ENABLE (1 << 1)

#define TIOCP_CFG_SOFTRESET (1 << 0)
#define TCLR_ST (1 << 0)
#define TCLR_AR (1 << 1)

struct timer_ctrl {
    uint32_t tidr;
    uint32_t res1[3];
    uint32_t tiocp_cfg;
    uint32_t res2[3];
    uint32_t irq_eoi;
    uint32_t irqstatus_raw;
    uint32_t irqstatus;
    uint32_t irqenable_set;
    uint32_t irqenable_clr;
    uint32_t irqwakeen;
    uint32_t tclr;
    uint32_t tcrr;
    uint32_t tldr;
    uint32_t ttgr;
    uint32_t twps;
    uint32_t tmar;
    uint32_t tcar1;
    uint32_t tsicr;
    uint32_t tccar2;
};

static const int intc_timers[] = {

    [DMTIMER_2] = INTC_TIMER2,
    [DMTIMER_3] = INTC_TIMER3,
    [DMTIMER_4] = INTC_TIMER4,
    [DMTIMER_5] = INTC_TIMER5,
    [DMTIMER_6] = INTC_TIMER6,
    [DMTIMER_7] = INTC_TIMER7};

struct listener {
    dmtimer_service_routine_t routine;
    void* param;
    uint32_t period;
    enum dmtimer_timers timer;
    volatile struct timer_ctrl* ctrl;
};

static volatile struct timer_ctrl* timer_ctrl[] = {
    [DMTIMER_2] = (volatile struct timer_ctrl*)0x48040000,
    [DMTIMER_3] = (volatile struct timer_ctrl*)0x48042000,
    [DMTIMER_4] = (volatile struct timer_ctrl*)0x48044000,
    [DMTIMER_5] = (volatile struct timer_ctrl*)0x48046000,
    [DMTIMER_6] = (volatile struct timer_ctrl*)0x48048000,
    [DMTIMER_7] = (volatile struct timer_ctrl*)0x4804a000,
};

static const enum am335x_clock_timer_modules timer2clock[] = {
    [DMTIMER_2] = AM335X_CLOCK_TIMER2,
    [DMTIMER_3] = AM335X_CLOCK_TIMER3,
    [DMTIMER_4] = AM335X_CLOCK_TIMER4,
    [DMTIMER_5] = AM335X_CLOCK_TIMER5,
    [DMTIMER_6] = AM335X_CLOCK_TIMER6,
    [DMTIMER_7] = AM335X_CLOCK_TIMER7};

struct listener listeners[] = {
    [DMTIMER_2] =
        {
            .timer = DMTIMER_2,
            .ctrl  = (struct timer_ctrl*)0x48040000,
        },
    [DMTIMER_3] =
        {
            .timer = DMTIMER_3,
            .ctrl  = (struct timer_ctrl*)0x48042000,
        },
    [DMTIMER_4] =
        {
            .timer = DMTIMER_4,
            .ctrl  = (struct timer_ctrl*)0x48044000,
        },
    [DMTIMER_5] =
        {
            .timer = DMTIMER_5,
            .ctrl  = (struct timer_ctrl*)0x48046000,
        },
    [DMTIMER_6] =
        {
            .timer = DMTIMER_6,
            .ctrl  = (struct timer_ctrl*)0x48048000,
        },
    [DMTIMER_7] =
        {
            .timer = DMTIMER_7,
            .ctrl  = (struct timer_ctrl*)0x4804a000,
        },
};

void dmtimer_set_period(enum dmtimer_timers timer, uint32_t period_ms)
{
    struct listener* l = &listeners[timer];

    l->period     = period_ms * timer_get_frequency();
    l->ctrl->tmar = 0;
    l->ctrl->tldr = -(l->period);
    l->ctrl->tcrr = -(l->period);
    l->ctrl->ttgr = -(l->period);
    l->ctrl->tclr = 0;
}

uint32_t dmtimer_get_period(enum dmtimer_timers timer)
{
    volatile struct timer_ctrl* ctrl = timer_ctrl[timer];
    return (ctrl->tcrr) / timer_get_frequency();
}

static void timer_handler(enum intc_vectors vector_nr, void* param)
{
    (void)vector_nr;
    struct listener* l = param;
    l->ctrl->irqstatus = IRQ_STATUS;
    if (l->routine != 0) {
        l->routine(l->param);
    } else {
        l->ctrl->irqenable_clr = IRQ_ENABLE;
        l->ctrl->tclr &= ~TCLR_ST;
    }
}

int dmtimer_on_event(enum dmtimer_timers timer,
                     dmtimer_service_routine_t routine,
                     void* param)
{
    struct listener* l = &listeners[timer];
    if (routine == 0) {
        l->ctrl->irqenable_clr = IRQ_ENABLE;
        intc_on_event(intc_timers[timer], 0, 0);
        l->routine = 0;
        l->param   = 0;
        return 0;
    }
    if (l->routine == 0) {
        l->routine = routine;
        l->param   = param;
        return intc_on_event(intc_timers[timer], timer_handler, l);
    }
    return -1;
}

void dmtimer_reset(enum dmtimer_timers timer)
{
    am335x_clock_enable_timer_module(timer2clock[timer]);
    volatile struct timer_ctrl* ctrl = timer_ctrl[timer];
    ctrl->tiocp_cfg                  = TIOCP_CFG_SOFTRESET;
    while ((ctrl->tiocp_cfg & TIOCP_CFG_SOFTRESET) != 0)
        ;

    ctrl->tldr          = 0;
    ctrl->tcrr          = 0;
    ctrl->ttgr          = 0;
    ctrl->irqenable_set = IRQ_ENABLE;
    ctrl->tclr          = TCLR_AR;
}

void dmtimer_start(enum dmtimer_timers timer)
{
    timer_ctrl[timer]->tclr = TCLR_AR | TCLR_ST;
}

void dmtimer_stop(enum dmtimer_timers timer)
{
    timer_ctrl[timer]->tclr &= ~TCLR_ST;
}

uint32_t timer_get_frequency() { return 24000; }
