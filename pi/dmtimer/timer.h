#pragma once
#ifndef TIMER_H_
#define TIMER_H_
/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 3 Laboratory
 *
 * Abstract:    Peripheral devices implementation with C
 *
 * Purpose:     Timer that does the job for the metronomelab
 *
 * Author: 		Bompard Corentin & Hoxha Arton
 *
 * Date: 		25.03.2020
 */
#include <stdint.h>

enum dmtimer_timers {
    DMTIMER_2,
    DMTIMER_3,
    DMTIMER_4,
    DMTIMER_5,
    DMTIMER_6,
    DMTIMER_7,
};

typedef void (*dmtimer_service_routine_t) (void* param);


uint32_t timer_get_frequency();

/**
 * method to initialize a DMTimer for interrupt service handling
 * an event will be generated at specified period interval
 *
 * @param timer DMTimer number
 * @param period_ms period in milliseconds
 */
extern void dmtimer_set_period(enum dmtimer_timers timer, uint32_t period_ms);

/**
 * method to get the timer period interval
 *
 * @param timer DMTimer number
 * @return period in milliseconds
 */
extern uint32_t dmtimer_get_period(enum dmtimer_timers timer);

/**
 * method to hook and unhook an timer service routine to a specified DMTimer
 *
 * @param timer DMTimer number
 * @param routine interrupt service routine to hook to the specified
 *                interrupt source, use 0 to unhook the method
 * @param param parameter to be passed as argument while calling the
 *              specified interrupt service routine
 * @return execution status, 0 if success, -1 if already attached
 */
extern int dmtimer_on_event(enum dmtimer_timers timer,
                            dmtimer_service_routine_t routine,
                            void* param);

/**
 * method to start the timer
 *
 * @param timer DMTimer number
 */
extern void dmtimer_start(enum dmtimer_timers timer);

/**
 * method to stop the timer
 *
 * @param timer DMTimer number
 */
extern void dmtimer_stop(enum dmtimer_timers timer);

extern void dmtimer_reset(enum dmtimer_timers timer);

#endif
