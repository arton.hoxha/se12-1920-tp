#include <stdio.h>

#include "counter.h"
#include "exception.h"
#include "game.h"
#include "gpio.h"
#include "intc.h"
#include "interrupt.h"
#include "kernel.h"
#include "lcd.h"
#include "leds.h"
#include "msgq.h"
#include "seg7.h"
#include "sema.h"
#include "thread.h"
#include "thread_menu.h"
#include "thread_render.h"
#include "timer.h"
#include "wheel.h"
#define SW_GPIO AM335X_GPIO1
#define WHEEL_SW_GPIO AM335X_GPIO0
#define WHEEL_SW_PIN 2
#define S1_PIN 15
#define BLINK_PERIOD 100
#define GAME_PERIOD 10

enum dmtimer_timers timer_blink = DMTIMER_2;
enum dmtimer_timers timer_game  = DMTIMER_3;

static void s1_handler(void* param)
{
    game* game = (struct game*)param;

    if (game->inSelection && game->selected_word[0] &&
        game->selected_word[0] != ' ') {
        leds_set_state(LED_1, true);
        msgq_post(game->events_msq, (void*)GAME_BTN_CLICK);
    } else {
        leds_set_state(LED_1, false);
    }
}

static void wheel_handler(enum wheel_direction dir, void* param)
{
    struct game* game = (struct game*)param;
    if (dir == RIGHT) {
        msgq_post(game->events_msq, (void*)GAME_WHEEL_RIGHT);
    } else {
        msgq_post(game->events_msq, (void*)GAME_WHEEL_LEFT);
    }
}

static void wheel_click(void* param)
{
    struct game* game = (struct game*)param;
    msgq_post(game->events_msq, (void*)GAME_WHEEL_CLICK);
}

void refresh_display(void* param)
{
    game* game = (struct game*)param;
    if (game->inSelection || game->inMenu || game->inGame) {
        static int blinkOn = 0;
        if (blinkOn == 0) {
            msgq_post(game->events_msq, (void*)GAME_BLINK_ON);

        } else if (blinkOn == 7) {
            msgq_post(game->events_msq, (void*)GAME_BLINK_OFF);
            leds_set_state(LED_1, false);
        }
        blinkOn = (blinkOn + 1) % 10;
    }
}

void refresh_counter(void* param)
{
    game* game = (struct game*)param;
    if (game->inGame) {
        static int seconds = 0;
        if (seconds == 0) {
            counter_display(game->counter);
            game->counter--;
        }
        seconds = (seconds + 1) % 100;
        if (game->counter == 0) {
            reset_counter();
            msgq_post(game->events_msq, (void*)GAME_STOP);
        }
        seg7_refresh_display();
    }
}

void init()
{
    counter_init();  // init wheel and seg7 too
    lcd_init();
    intc_init();
    interrupt_init();
    exception_init();
    gpio_init();
    leds_init();
    kernel_init();
}

int main()
{
    init();
    struct game game;
    game_init(&game);

    dmtimer_reset(timer_game);
    dmtimer_on_event(timer_game, refresh_counter, &game);
    dmtimer_set_period(timer_game, GAME_PERIOD);
    dmtimer_start(timer_game);

    dmtimer_reset(timer_blink);
    dmtimer_on_event(timer_blink, refresh_display, &game);
    dmtimer_set_period(timer_blink, BLINK_PERIOD);
    dmtimer_start(timer_blink);

    gpio_on_event(SW_GPIO, S1_PIN, GPIO_FALLING, s1_handler, &game);
    gpio_on_event(WHEEL_SW_GPIO,
                  WHEEL_SW_PIN,
                  GPIO_FALLING | GPIO_DEBOUNCED,
                  wheel_click,
                  &game);

    wheel_on_event(wheel_handler, &game);
    interrupt_enable(INTERRUPT_ALL);
    thread_create(thread_game, &game, "game", 0);
    thread_create(thread_render, &game, "lcd", 0);
    thread_create(thread_menu, &game, "menu", 0);
    kernel_launch();
}
