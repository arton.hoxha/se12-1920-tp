/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Abstract:    Introduction to C language and modular programming
 *
 * Purpose:     Set and get the state of leds
 *
 * Author:      <Arton Hoxha & Valentin Antille>
 *
 * Date:        <01.10.2019>
 */

#include "leds.h"

#include <bbb/am335x_gpio.h>
#include <stdbool.h>
#include <stdint.h>

// pin constants definition
#define GPIO1 AM335X_GPIO1
#define LED1 12
#define LED2 13
#define LED3 14

// array which corresponds LED enums to pins
static const uint32_t lut[] = {[LED_1] = LED1, [LED_2] = LED2, [LED_3] = LED3};

// initialize the leds
void leds_init()
{
    am335x_gpio_init(GPIO1);
    am335x_gpio_setup_pin_out(GPIO1, LED1, false);
    am335x_gpio_setup_pin_out(GPIO1, LED2, false);
    am335x_gpio_setup_pin_out(GPIO1, LED3, false);
}

// get the state of a led
bool leds_get(enum leds_id led)
{
    return am335x_gpio_get_state(GPIO1, lut[led]);
}

// set the state of a led
void leds_set_state(enum leds_id led, bool state)
{
    am335x_gpio_change_state(GPIO1, lut[led], state);
}
