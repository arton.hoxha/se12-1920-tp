#pragma once
#ifndef LEDS_H
#define LEDS_H
/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Abstract:    Introduction to C language and modular programming
 *
 * Purpose:     Set and get the state of leds
 *
 * Author:      <Arton Hoxha & Valentin Antille>
 *
 * Date:        <01.10.2019>
 */

#include <stdbool.h>

// enum leds for better readability
enum leds_id { LED_1, LED_2, LED_3 };

/**
 * initialisation of leds
 */
void leds_init();

/**
 * get the state of one led
 */
bool leds_get(enum leds_id led);

/**
 * set the state of one led
 */
void leds_set_state(enum leds_id led, bool state);
#endif
