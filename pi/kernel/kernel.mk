EXEC=app
SRCS=$(wildcard *.[cSs])

CFLAGS+=-I../interrupt
LDFLAGS += -l interrupt -L../interrupt

# Include the standard makefile for the embedded systems 1 & 2 labs
include ../../make/bbb.mk
CFLAGS+=-Wno-unused
