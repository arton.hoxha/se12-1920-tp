/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 2
 *
 * Abstract:    Introduction to Operating System
 *
 * Purpose:     Micro kernel service definition
 *
 * Author:      Daniel Gachet / HEIA-FR
 * Date:        05.03.2020
 */

#include "thread.h"

#include <bbb/am335x_dmtimer1.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#define THREAD_REGS 15
#define THREAD_NAME_LEN 32
#define THREAD_MIN_STACK_SIZE 0x4000

// definition of the possible states of a thread
enum thread_states {
    THREAD_READY,
    THREAD_RUNNING,
    THREAD_BLOCKED,
    THREAD_DELAYED,
    THREAD_TERMINATED,
};

// thread context
struct thread_context {
    uint32_t regs[THREAD_REGS];
    uint32_t psr;
};

// definition of the thread control block (tcb)
struct thread_tcb {
    struct thread_context context;
    enum thread_states state;

    struct thread_tcb* t_next;  // used to chain all tcb
    struct thread_tcb* b_next;  // used to chain blocked threads
    struct thread_tcb* d_next;  // used to chain delayed threads
    uint64_t wake_up_time;      // wake up time in ticks

    int32_t id;                  // unique thread identification
    char name[THREAD_NAME_LEN];  // thread name

    uint32_t stack_size;  // thread stack size in bytes
    uint32_t stack[];     // stack
};

static struct thread_tcb* delayed_threads;
static struct thread_tcb* running_thread;

static struct thread_tcb idle_thread = {
    // kernel idle thread, This is also the head of the threads list
    .state      = THREAD_RUNNING,
    .t_next     = &idle_thread,
    .id         = 0,
    .name       = "idle",
    .stack_size = 0,
};

extern void thread_transfer(struct thread_context* former,
                            struct thread_context* new);

// --- implementation of local services / methods -----------------------------

static int32_t new_thread_id()
{
    // Use a static variable to store the last id given
    // The first id returned by this function should be 1,
    // because 0 is reserved the the "idle" process.
    static int id = 0;
    return ++id;
}

static void thread_startup(void* param, thread_t thread)
{
    // thread(param)
    thread(param);
    // thread_exit()
    thread_exit();
}

static struct thread_tcb* new_tcb(uint32_t stack_sz, const char* name)
{
    // Allocate memory for the tcb and for the stack.
    struct thread_tcb* tcb = malloc(sizeof(struct thread_tcb) + stack_sz);

    if (tcb != 0) {
        memset(tcb, 0, sizeof(struct thread_tcb));
        tcb->stack_size = stack_sz;
        tcb->id         = new_thread_id();
        strncpy(tcb->name, name, sizeof(tcb->name) - 1);
    }

    return tcb;
}

static void insert_tcb(struct thread_tcb* tcb)
{
    // Insert the tcb at the beginning of the chain starting at "idle_task"
    tcb->t_next        = idle_thread.t_next;
    idle_thread.t_next = tcb;
}

// --- utility threads ---

static void process_terminated_threads(void* param)
{
    // while true
    //      Remove all threads (tcb) having a terminated state
    //      don't forget to "free" the memory as well.
    //      yield()
    (void)param;
    while (true) {
        struct thread_tcb* head = &idle_thread;
        struct thread_tcb* prev = head;
        // I will never remove the idle_thread!
        while (prev->t_next != head) {
            struct thread_tcb* actual = prev->t_next;
            if (actual != running_thread &&
                actual->state == THREAD_TERMINATED) {
                // I will never, never, never remove myself!
                prev->t_next = actual->t_next;
                free(actual);
            } else {
                prev = actual;
            }
        }

        thread_yield();
    }
}

static void process_delayed_threads(void* param)
{
    (void)param;

    // while true
    while (true) {
        // get current uptime value
        uint64_t uptime = am335x_dmtimer1_get_uptime();
        // for all delayed threads
        struct thread_tcb* n  = delayed_threads;
        struct thread_tcb** i = &delayed_threads;

        while (n != 0) {
            // If the "wake_up_time" attribute of the field is <= uptime:
            if (n->wake_up_time <= uptime) {
                // Wake-up the thread by reseting the wake_up_time
                n->wake_up_time = 0;
                // attribute to 0 and set the state to ready
                n->state = THREAD_READY;
                *i       = n->d_next;
            } else {
                i = &(n->d_next);
            }
            n = n->d_next;
        }
        // yield()
        thread_yield();
    }
}

// --- implementation of public services / methods ----------------------------

int thread_create(thread_t thread,
                  void* param,
                  const char* name,
                  uint32_t stack_size)
{
    // Set the minumum stack size to 16KB
    stack_size = (stack_size < THREAD_MIN_STACK_SIZE)
                     ? THREAD_MIN_STACK_SIZE + 8
                     : (stack_size + 8);

    // Create a new tcb
    struct thread_tcb* tcb = new_tcb(stack_size, name);
    if (tcb == 0) return -1;

    // Set r0 of the context of the new tcb to the address of the param
    tcb->context.regs[0] = (uint32_t)param;
    // Set r1 of the context of the new tcb to the address of the thread
    tcb->context.regs[1] = (uint32_t)thread;
    // Set sp of the context of the new tcb to the end of the stack.
    // Make sure that it is at a multiple of 8
    tcb->context.regs[13] = (((uint32_t)&tcb->stack[stack_size / 4]) / 8) * 8;
    // Set lr of the context of the new tcb at the address of "thread_startup"
    tcb->context.regs[14] = (uint32_t)thread_startup;
    // Set cpsr of the context of the new tcb to supervisor mode,
    // with IRQ and FIQ enabled
    tcb->context.psr = 0x13;  // mode: supervisor; IRQ & FIQ enabled
    // Set the state of the new tcb to ready
    tcb->state = THREAD_READY;

    // add the tcb to the chain
    insert_tcb(tcb);
    // Return the id of the added tcb
    return tcb->id;
}

void thread_yield()
{
    struct thread_tcb* former_thread = running_thread;
    struct thread_tcb* new_thread    = former_thread->t_next;

    // Set the current task to ready if it is running.
    if (former_thread->state == THREAD_RUNNING)
        former_thread->state = THREAD_READY;

    // Look for a task having a state of ready (starting from
    // the task that directly follow the current task).
    while (new_thread->state != THREAD_READY) {
        new_thread = new_thread->t_next;
    }

    // Set "running_task" to the task that we just found.
    running_thread = new_thread;
    // Set the state of the new running task to running
    new_thread->state = THREAD_RUNNING;
    // Call the assembler method "thread_transfer" to do the "real" tranfer.
    thread_transfer(&former_thread->context, &new_thread->context);
}

void thread_delay(uint32_t ms)
{
    // Get the tcb of the running task
    struct thread_tcb* th = running_thread;

    // Set the wake_up_time attribute of the tcb to wake-up time
    th->wake_up_time = am335x_dmtimer1_get_uptime() +
                       ((am335x_dmtimer1_get_frequency() / 1000) * ms);
    // Set the state of the tcb to "delayed"
    th->state = THREAD_DELAYED;
    // Add the tcb the the "delayed_threads" list
    th->d_next      = delayed_threads;
    delayed_threads = th;

    // yield()
    thread_yield();
}

void thread_exit()
{
    // Set the state of the running task to thread_TERMINATED
    running_thread->state = THREAD_TERMINATED;
    // yield()
    thread_yield();
}

const char* thread_name()
{
    // Returns the name of the running task
    return running_thread->name;
}

int thread_id()
{
    // Return the id of the running task
    return running_thread->id;
}

void thread_release(void** chain)
{
    struct thread_tcb** tcb = (struct thread_tcb**)chain;

    // if thread pending in the chain
    if (*tcb != 0) {
        // change thread state to ready
        (*tcb)->state = THREAD_READY;
        // extract thread out of pending chain
        (*tcb) = (*tcb)->b_next;
    }
}

void thread_block(void** chain)
{
    struct thread_tcb** tcb = (struct thread_tcb**)chain;

    // add thread at end of pending chain
    while (*tcb != 0) {
        tcb = &(*tcb)->b_next;
    }
    *tcb                   = running_thread;
    running_thread->b_next = 0;

    // change thread state to blocked
    running_thread->state = THREAD_BLOCKED;
}

// ----------------------------------------------------------------------------
// --- micro kernel thread library initialization ------------------------------
// ----------------------------------------------------------------------------

void thread_init()
{
    running_thread = &idle_thread;
    // Create a thread with "terminated_threads"
    thread_create(process_terminated_threads, (void*)0, "terminated", 0);
    // Create a thread with "process_delayed_threads" --> 2nd phase
    thread_create(process_delayed_threads, (void*)0, "delayed", 0);
}
