/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: HEIA-FR / Embedded Systems 2
 *
 * Abstract: Introduction to Operating System
 *
 * Purpose: Micro kernel service definition
 *
 * Author:  Daniel Gachet / HEIA-FR
 * Date:    05.03.2020
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "thread.h"
#include "../interrupt/interrupt.h"
#include "sema.h"

// ----------------------------------------------------------------------------
// --- semaphore service implementation ---------------------------------------
// ----------------------------------------------------------------------------

struct sema {
    int32_t count;
    void* t_chain;
    struct sema* chain;
};

static struct sema* sema_list = 0;

// --- public services ---

semaid_t sema_create(int initial_value)
{
    // Allocate memory for a new semaphore
    struct sema* sema = calloc(1, sizeof(*sema));
    if (sema == 0)
    {
        return 0;
    }

    // Set the counter attribute of the semaphore the the initial value
    sema->count = initial_value;
    sema->t_chain = 0;

    // Add the semaphore to the "sema_list" list
    sema->chain = sema_list;
    sema_list = sema;

    // Returns the semaphore
    return (semaid_t)sema;
}

int sema_destroy(semaid_t sema_id)
{
    struct sema* sema = (struct sema*)sema_id;

    // If threads are still waiting for the semaphore:
    //   Return -1
    if (sema->t_chain != 0)
    {
        return -1;
    }

    // Remove the semaphore from the list
    struct sema** indirect = &sema_list;
    while ((*indirect) != sema)
    {
        indirect = &(*indirect)->chain;
    }
    *indirect = sema->chain;

    // Free the memory
    free(sema);
    // Return 0
    return 0;
}

void sema_signal(semaid_t sema_id)
{
    struct sema* sema = (struct sema*)sema_id;


    // Disable the interrupts to protect the critical section
    int flags = interrupt_disable(INTERRUPT_ALL);

    // Increment the counter of the semaphore
    sema->count++;

    // If process are waiting for this semaphore:
    //   call thread_release()
    thread_release(&sema->t_chain);
    // Re-enable the interrupts as before the call
    interrupt_setflags(flags);
}

void sema_wait(semaid_t sema_id)
{
    struct sema* sema = (struct sema*)sema_id;

    // Disable the interrupts to protect the critical section
    int flags = interrupt_disable(INTERRUPT_ALL);
    // Decrement the counter of the semaphore
    sema->count--;

    // If the counter is negative:
    //    call thread_block
    if (sema->count < 0)
    {
        thread_block(&sema->t_chain);
    }

    // Re-enable the interrupts as before the call
    interrupt_setflags(flags);
    // call yield() ! IMPORTANT !
    thread_yield();
}

int sema_trywait(semaid_t sema_id)
{
    struct sema* sema = (struct sema*)sema_id;
    // status = error
    int ok = -1;
    // Disable the interrupts to protect the critical section
    int flags = interrupt_disable(INTERRUPT_ALL);

    // If the counter is greather than 0:
    //    Decrement the counter of the semaphore
    //    status = ok
    if (sema->count > 0) {
        sema->count--;
        ok = 0;
    }
    // Re-enable the interrupts as before the call
    interrupt_setflags(flags);

    // return status
    return ok;

}

int sema_count(semaid_t sema_id)
{
    struct sema* sema = (struct sema*)sema_id;
    return sema->count;
}

// ----------------------------------------------------------------------------
// --- micro kernel sema library initialization -------------------------------
// ----------------------------------------------------------------------------

void sema_init()
{
    // Nothing to do
}
