/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 2
 *
 * Abstract:    Introduction to Operating System
 *
 * Purpose:     Micro kernel service definition
 *
 * Autĥor:      Daniel Gachet / HEIA-FR / Telecoms
 * Date:        05.03.2020
 */

#include "msgq.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../interrupt/interrupt.h"
#include "sema.h"

struct msgq {
    struct msgq* chain;

    semaid_t posted_msg;
    semaid_t free_space;

    bool blocking;
    unsigned size;
    unsigned in;
    unsigned out;
    void* msg[];
};

static struct msgq* msgq_list = 0;

msgqid_t msgq_create(unsigned size, bool blocking)
{
    // allocate memory for the whole queue (for size messages)
    struct msgq* msgq = calloc(1, sizeof(struct msgq) + sizeof(void*) * size);
    if (msgq == 0)
    {
        return 0;
    }

    // set the size attribute
    msgq->size = size;
    // create "posted_msg" as a new semaphore with initial value of 0
    msgq->posted_msg = sema_create(0);
    // create "free_space" as a new semaphore with initial value of size
    msgq->free_space = sema_create(size);
    msgq->blocking = blocking;

    // add message queue to the list
    msgq->chain = msgq_list;
    msgq_list = msgq;

    // return the created message queue
    return (msgqid_t)msgq;
}

int msgq_destroy(msgqid_t msgq_id)
{
    struct msgq* msgq = (struct msgq*)msgq_id;

    // destroy all semaphores (returns error code on failure)
    if (sema_destroy(msgq->posted_msg) != 0)
    {
        return -1;
    }

    if (sema_destroy(msgq->free_space) != 0)
    {
        return -1;
    }

    // remove the message queue from the "msgq_list" list
    struct msgq** indirect = &msgq_list;
    while ((*indirect) != msgq)
    {
        indirect = &(*indirect)->chain;
    }
    *indirect = msgq->chain;

    // free memory
    free(msgq);
    // return 0
    return 0;
}

int msgq_post(msgqid_t msgq_id, void* msg)
{
    struct msgq* msgq = (struct msgq*)msgq_id;

    // if no free space availabe in msgq (sema_trywait "free_space")
    //    if non-blocking msgq, then return an error
    //    other wait "free_space"
    if (sema_trywait(msgq->free_space) == -1)
    {
        if (!msgq->blocking)
        {
            return -1;
        }
        sema_wait(msgq->free_space);
    }

    // disable all interrupts (avoid interrupts during handling)
    int flags = interrupt_disable(INTERRUPT_ALL);
    //      Add msg to the list (at "in")
    msgq->msg[msgq->in++] = msg;

    if (msgq->in >= msgq->size)
    {
        msgq->in = 0;
    }
        // restore interrupt mask
    interrupt_setflags(flags);
    // signal "posted_msg"
    sema_signal(msgq->posted_msg);
    return 0;
}

void* msgq_fetch(msgqid_t msgq_id)
{
    struct msgq* msgq = (struct msgq*)msgq_id;
    // wait for the "posted_msg" semaphore (prevent underflow)
    sema_wait(msgq->posted_msg);
    // disable all interrupts (avoid interrupts during handling)
    int flags = interrupt_disable(INTERRUPT_ALL);
    //      get msg from the list (at "out")
    void* msg = msgq->msg[msgq->out++];
    if (msgq->out >= msgq->size)
    {
        msgq->out = 0;
    }
    // restore interrupt mask
    interrupt_setflags(flags);
    // signal "free_space"
    sema_signal(msgq->free_space);

    return msg;
}

void msgq_init()
{
    // Nothing to do
}
