#include "thread_render.h"

#include "../game/end_img.h"
#include "../game/game.h"
#include "../game/hangman.h"
#include "../game/main_menu.h"
#include "../kernel/thread.h"
#include "../lcd/lcd.h"
#include "stdint.h"
#include "stdio.h"
#include "string.h"

// Hangman position
#define HANGMAN_X 20
#define HANGMAN_Y 20

// Colors
#define WHITE 0xFFFF
#define BLACK 0x0000

// Array Size
#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

// Constants
#define WORD_LENGTH 9
#define TITLE_Y 20
#define TICK_WORD_X1 80
#define TICK_WORD_X2 82
#define TICK_WORD_Y1 4
#define TICK_WORD_Y2 12
#define TICK_WORD_Y_OFFSET 10
#define MENU_IMAGE_POS 10
#define POS_0 0
#define SCREEN_WIDTH 96
#define SCREEN_HEIGHT 96
#define OFFSET_SCREEN 16
#define SELECTION_TICK_X 48
#define SELECTION_TICK_Y 4
#define SELECTION_TICK_X_OFFSET 6
#define SELECTION_TICK_Y_OFFSET 4
#define CHAR_X 40
#define CHAR_Y 4
#define CHAR_OFFSET 10
#define X_CLEAR_PIXELS_IMG 20
#define Y_CLEAR_PIXELS_IMG 24
#define WELCOMESTR_POS_Y 25
#define CLICKWHEELSTR_POS_X 85
#define CLICKWHEELSTR_POS_Y 5
#define PLAYER1STR_POS_Y 20
#define SELECTED_WORD_STR_POS_X 12
#define RENDER_TICK_X_OFFSET 2
#define RENDER_TICK_Y_OFFSET 10
#define RENDER_TICK_OFFSET 3

char* select_word_str   = "Select word:";
char* player1_str       = "Player 1";
char* selected_word_str = "Your word:";
char* hangman_str       = "Hangman";

static void render_main_menu()
{
    lcd_draw_image(&main_menu, MENU_IMAGE_POS, MENU_IMAGE_POS);
    lcd_display_string("Welcome!", POS_0, WELCOMESTR_POS_Y, false);
    lcd_display_string(
        "Click wheel ", CLICKWHEELSTR_POS_X, CLICKWHEELSTR_POS_Y, false);
}

static void render_selection()
{
    // Screen clear
    lcd_draw_rect(
        POS_0, SCREEN_HEIGHT - OFFSET_SCREEN, POS_0, SCREEN_WIDTH - 1, BLACK);
    lcd_draw_rect(SCREEN_HEIGHT - OFFSET_SCREEN,
                  SCREEN_HEIGHT - 1,
                  POS_0,
                  SCREEN_WIDTH - 1,
                  BLACK);
    lcd_display_string(player1_str, POS_0, PLAYER1STR_POS_Y, false);
    lcd_display_string(
        selected_word_str, SELECTED_WORD_STR_POS_X, POS_0, false);
    for (int i = 0; i < WORD_LENGTH; i++) {
        lcd_draw_rect(
            SELECTION_TICK_X,
            SELECTION_TICK_X + RENDER_TICK_X_OFFSET,
            SELECTION_TICK_Y + RENDER_TICK_Y_OFFSET * i,
            SELECTION_TICK_Y * RENDER_TICK_OFFSET + RENDER_TICK_Y_OFFSET * i,
            WHITE);
    }
}

static void render_game(game* game)
{
    lcd_change_text_color(WHITE);
    // clear screen
    lcd_draw_rect(
        POS_0, SCREEN_WIDTH - OFFSET_SCREEN, POS_0, SCREEN_WIDTH - 1, BLACK);
    lcd_display_string(hangman_str, POS_0, TITLE_Y, false);
    // tick selection
    lcd_draw_rect(SELECTION_TICK_X - SELECTION_TICK_X_OFFSET,
                  SELECTION_TICK_X - SELECTION_TICK_Y_OFFSET,
                  SELECTION_TICK_Y,
                  SELECTION_TICK_Y * RENDER_TICK_OFFSET,
                  WHITE);
    // draw word ticks
    for (int i = 0; i < game->selected_word_length; i++) {
        lcd_draw_rect(TICK_WORD_X1,
                      TICK_WORD_X2,
                      TICK_WORD_Y1 + TICK_WORD_Y_OFFSET * i,
                      TICK_WORD_Y2 + TICK_WORD_Y_OFFSET * i,
                      WHITE);
    }
    // draw hangman first part
    lcd_draw_image(&hangman[0], HANGMAN_X, HANGMAN_Y);

    lcd_draw_rect(
        X_CLEAR_PIXELS_IMG, Y_CLEAR_PIXELS_IMG, POS_0, SCREEN_WIDTH - 1, BLACK);
}

static void render_word_char(game* game, int index)
{
    lcd_display_char(game->selected_word[index],
                     SCREEN_HEIGHT - OFFSET_SCREEN,
                     CHAR_Y + index * CHAR_OFFSET,
                     false);
}

void thread_render(void* param)
{
    game* game                    = (struct game*)param;
    uint32_t char_x               = CHAR_X;
    uint32_t char_y               = CHAR_Y;
    static int hangman_part_index = 1;  // first part of the hangman
    while (1) {
        int msg = (int)msgq_fetch(game->render_msq);
        if (msg == RENDER_MAIN_MENU) {
            render_main_menu();
        } else if (msg == RENDER_END_MENU) {
            // Those two lines are used to reset the Lcd
            // because lcd_clear() method is too slow
            lcd_draw_rect(POS_0,
                          SCREEN_HEIGHT - OFFSET_SCREEN,
                          POS_0,
                          SCREEN_WIDTH - 1,
                          BLACK);
            lcd_draw_rect(SCREEN_HEIGHT - OFFSET_SCREEN,
                          SCREEN_HEIGHT - 1,
                          POS_0,
                          SCREEN_WIDTH - 1,
                          BLACK);
            msgq_post(game->menu_msq, (void*)MENU_RENDER);
        } else if (msg == RENDER_SELECTION) {
            render_selection();
        } else if (msg == RENDER_GAME) {
            render_game(game);
            char_x = CHAR_X;  // set the selection char in screen
            char_y = CHAR_Y;
        } else if (msg == RENDER_END) {
            lcd_draw_image(&end_img, POS_0, POS_0);
            msgq_post(game->events_msq, (void*)GAME_END);
        } else if (msg == RENDER_RESET) {
            hangman_part_index = 1;  // reset to first part
            char_y             = CHAR_Y;
            render_selection();
        } else if (msg == RENDER_WORD_CHAR) {
            for (int i = 0; i < WORD_LENGTH; i++) {
                if (game->are_chars_in_word[i]) {
                    render_word_char(game, i);
                }
            }
        } else if (msg == RENDER_NEXT_PART) {
            lcd_draw_image(&hangman[hangman_part_index], HANGMAN_X, HANGMAN_Y);
            lcd_draw_rect(X_CLEAR_PIXELS_IMG,
                          Y_CLEAR_PIXELS_IMG,
                          POS_0,
                          SCREEN_WIDTH - 1,
                          BLACK);
            hangman_part_index++;
        } else if (msg == RENDER_CHAR) {
            lcd_display_char(game->selected_char, char_x, char_y, false);
        } else if (msg == RENDER_NEXT_CHAR) {
            char_y += CHAR_OFFSET;
        } else if (msg == RENDER_REMOVE_CHAR) {
            lcd_remove_char(char_x, char_y);
        } else {
            thread_yield();
        }
    }
}
