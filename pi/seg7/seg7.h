#pragma once
#ifndef SEG7_H
#define SEG7_H
/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Abstract:    Introduction to C language and modular programming
 *
 * Purpose:     Define the specification to use the 7-segments display
 *
 * Author:      <Arton Hoxha & Valentin Antille>
 * Date:        <01.10.2019>
 */

#include <stdbool.h>
#include <stdint.h>

// enum segments display for better readability
enum seg7_display { SEG7_LEFT, SEG7_RIGHT };

// enum segments for better readability
enum seg7_segments {
    SEG7_A = (1 << 0),
    SEG7_B = (1 << 1),
    SEG7_C = (1 << 2),
    SEG7_D = (1 << 3),
    SEG7_E = (1 << 4),
    SEG7_F = (1 << 5),
    SEG7_G = (1 << 6),
};

/**
 * initialize the 7 segments displays
 */
void seg7_init();

/**
 * turn off or on every one of the 7 segments
 */
void display_segments(enum seg7_display digit, uint32_t value);

/**
 * turn off or on the dots
 */
void display_dots(bool show_dot);

/**
 * alternate between the right and left side to give the illusion to display
 * elements on both sides at the same time
 */
void seg7_refresh_display();

#endif
