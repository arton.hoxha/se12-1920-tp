/**
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This module is based on the software library developped by Texas Instruments
 * Incorporated - http://www.ti.com/ for its AM335x starter kit.
 *
 * Project:	HEIA-FR / Embeed Systems 1 Laboratory 3
 *
 *
 * Purpose:	This module implement the display on the LCD screen
 *
 * Author: 	Corentin Bompard & Lucas Bueche
 * Date: 	05.11.2019
 */

#include "lcd.h"

#include <bbb/font_8x8.h>
#include <bbb/oled.h>
#include <stdbool.h>
#include <string.h>

#define VERSION OLED_V101
#define FONT_SIZE 8
#define DISPLAY_SIZE 96

#define ARRAY_SIZE(x) ((signed)(sizeof(x) / sizeof(x[0])))

// local variable ----------------------------------------------------------

static uint16_t text_color       = 0xffff;  // base white
static uint16_t background_color = 0x0000;  // base black

// local method ------------------------------------------------------------

// rotates the pixels 90 degree left
static void rotate(struct pixel_b* tab,
                   int width,
                   int height,
                   struct pixel_b* rotated)
{
    struct pixel_b matrix[height][width];
    struct pixel_b matrixRotated[height][width];

    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            matrix[i][j] = tab[i * width + j];
        }
    }
    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            matrixRotated[(height - 1) - j][i] = matrix[i][j];
        }
    }

    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            rotated[i * height + j] = matrixRotated[i][j];
        }
    }
}

// public method implementation --------------------------------------------

void lcd_init()
{
    oled_init(VERSION);
    lcd_clear();
}

void lcd_reset_color()
{
    text_color       = 0xffff;
    background_color = 0x0000;
}

void lcd_change_background_color(int color) { background_color = color; }

void lcd_change_text_color(int color) { text_color = color; }

void lcd_clear()
{
    oled_memory_size(0, DISPLAY_SIZE - 1, 0, DISPLAY_SIZE - 1);
    struct pixel_b clear[DISPLAY_SIZE * DISPLAY_SIZE];

    for (int i = 0; i < ARRAY_SIZE(clear); i++) {
        clear[i].hword = background_color >> 8;
        clear[i].lword = background_color & 0xff;
    }

    oled_send_image_b(clear, ARRAY_SIZE(clear));
}

void lcd_display_char(char c, uint32_t x, uint32_t y, bool inverted)
{
    oled_memory_size(x, x + FONT_SIZE - 1, y, y + FONT_SIZE - 1);

    struct pixel_b pixel[FONT_SIZE * FONT_SIZE];

    for (int i = 0; i < FONT_SIZE; i++) {
        for (int j = 0; j < FONT_SIZE; j++) {
            if (!inverted) {
                pixel[FONT_SIZE * i + j].hword =
                    ((fontdata_8x8[(int)c][i] >> j) & 1)
                        ? (text_color >> 8)
                        : (background_color >> 8);
                pixel[FONT_SIZE * i + j].lword =
                    ((fontdata_8x8[(int)c][i] >> j) & 1)
                        ? (text_color & 0xff)
                        : (background_color & 0xff);
            } else {
                pixel[FONT_SIZE * i + j].hword =
                    ((fontdata_8x8[(int)c][i] >> j) & 1)
                        ? (background_color >> 8)
                        : (text_color >> 8);
                pixel[FONT_SIZE * i + j].lword =
                    ((fontdata_8x8[(int)c][i] >> j) & 1)
                        ? (background_color & 0xff)
                        : (text_color & 0xff);
            }
        }
    }

    struct pixel_b rotatedPixel[FONT_SIZE * FONT_SIZE];
    rotate(pixel, FONT_SIZE, FONT_SIZE, rotatedPixel);

    oled_send_image_b(rotatedPixel, FONT_SIZE * FONT_SIZE);
}

void lcd_display_string(char* s, uint32_t fromX, uint32_t fromY, bool inverted)
{
    oled_memory_size(0, DISPLAY_SIZE - 1, 0, DISPLAY_SIZE - 1);
    uint32_t y = fromY;
    char c     = *s;
    while (c != '\0') {
        if (c == '\n') {
            fromY = y;
            fromX += FONT_SIZE;
        } else {
            lcd_display_char(c, fromX, fromY, inverted);
            fromY += FONT_SIZE;
            if (fromY > DISPLAY_SIZE) {
                fromY -= DISPLAY_SIZE / 2;
                fromX += FONT_SIZE;
            }
        }
        c = *(++s);
    }
}

void lcd_draw_rect(
    uint32_t x1, uint32_t x2, uint32_t y1, uint32_t y2, int color)
{
    oled_memory_size(x1, x2 - 1, y1, y2 - 1);

    int width  = x2 - x1;
    int height = y2 - y1;

    struct pixel_b rect[width * height];

    for (int i = 0; i < ARRAY_SIZE(rect); i++) {
        rect[i].hword = color >> 8;
        rect[i].lword = color & 0xff;
    }

    oled_send_image_b(rect, ARRAY_SIZE(rect));
}

void lcd_draw_circle(
    uint32_t x, uint32_t y, int r, int fill_color, int back_color)
{
    oled_memory_size(x - r, x + r - 1, y - r, y + r - 1);

    int width  = 2 * r;
    int height = 2 * r;

    struct pixel_b circle[width * height];

    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            int dist_i = (i - width / 2) * (i - width / 2);
            int dist_j = (j - height / 2) * (j - height / 2);
            int dist   = dist_i + dist_j;
            if (dist <= r * r) {
                circle[i * width + j].hword = fill_color >> 8;
                circle[i * width + j].lword = fill_color & 0xff;
            } else {
                circle[i * width + j].hword = back_color >> 8;
                circle[i * width + j].lword = back_color & 0xff;
            }
        }
    }

    oled_send_image_b(circle, ARRAY_SIZE(circle));
}

int lcd_get_background_color() { return background_color; }

void lcd_remove_char(uint32_t x, uint32_t y)
{
    oled_memory_size(x, x + 6, y, y + 8);

    int width  = 8;
    int height = 8;

    struct pixel_b rect[width * height];

    for (int i = 0; i < ARRAY_SIZE(rect); i++) {
        rect[i].hword = background_color >> 8;
        rect[i].lword = background_color & 0xff;
    }

    oled_send_image_b(rect, ARRAY_SIZE(rect));
}

void lcd_remove_string(uint32_t x, uint32_t y, char* str)
{
    oled_memory_size(x, x + 8, y, y + 8 * strlen(str));

    int width  = 8 * strlen(str);
    int height = 8;

    struct pixel_b rect[width * height];

    for (int i = 0; i < ARRAY_SIZE(rect); i++) {
        rect[i].hword = background_color >> 8;
        rect[i].lword = background_color & 0xff;
    }

    oled_send_image_b(rect, ARRAY_SIZE(rect));
}

static struct pixel_b image_pixel_color(Image* image, int x, int y)
{
    int index = (x * image->width + y) * 2;

    struct pixel_b p = {
        .hword = image->pixel_data[index + 1],  // rgb565_value >> 8
        .lword = image->pixel_data[index]       // rgb565_value & 0xff
    };
    return p;
}

void lcd_draw_image(Image* image, int x, int y)
{
    int width   = image->width;
    int height  = image->height;
    int counter = 0;
    oled_memory_size(x, height + x - 1, y, width + y - 1);
    struct pixel_b img[width * height];

    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            img[counter] = image_pixel_color(image, j, i);
            counter++;
        }
    }

    oled_send_image_b(img, width * height);
}
