#pragma once
#ifndef LCD_H_
#define LCD_H_

/**
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This module is based on the software library developped by Texas Instruments
 * Incorporated - http://www.ti.com/ for its AM335x starter kit.
 *
 * Project:	HEIA-FR / Embedded Systems 1 Laboratory 3
 *
 *
 * Purpose:	This module implement the display on the LCD screen
 *
 * Author: 	Corentin Bompard & Lucas Bueche
 * Date: 	05.11.2019
 */
#include <stdbool.h>
#include <stdint.h>

#define LIGHT_BLUE 0x2ff
#define WHITE 0xFFFF
#define BLACK 0x0000
#define RED 0xF800

typedef struct Image {
    unsigned int width;
    unsigned int height;
    unsigned int bytes_per_pixel;
    unsigned char pixel_data[96 * 96 * 2 + 1];
} Image;

/**
 * Method to init the lcd screen
 */
extern void lcd_init();

/**
 * Method to clear the lcd when we change the menu
 */
extern void lcd_clear();

/**
 * Method to reset the color on the lcd screen
 */
extern void lcd_reset_color();

/**
 * Method to change the background color on the lcd screen
 * @param color color to display on the background
 */
extern void lcd_change_background_color(int color);

/**
 * Method to change the text color on the lcd screen
 * @param color color to display the text
 */
extern void lcd_change_text_color(int color);

/**
 * Method to display a char on the lcd screen
 * @param c char to display on the lcd screen
 * @param x x position of the char
 * @param y y position of the char
 * @param inverted to determine if the color is inverted or not
 */
extern void lcd_display_char(char c, uint32_t x, uint32_t y, bool inverted);

/**
 * Method to display a string on the lcd screen
 * @param s tab of char to make a string and display it on the lcd screen
 * @param fromX x position of the first char in the string
 * @param fromY y position of the first char in the string
 * @param inverted to determine if the color is inverted or not
 */
extern void lcd_display_string(char* s,
                               uint32_t fromX,
                               uint32_t fromY,
                               bool inverted);

/**
 * Method to draw a rectangle on the lcd screen
 * @param x1 x coordonate of the point 1
 * @param x2 x coordonate of the point 2
 * @param y1 y coordonate of the point 1
 * @param y2 y coordonate of the point 2
 * @param color color of the rectangle
 */
extern void lcd_draw_rect(
    uint32_t x1, uint32_t x2, uint32_t y1, uint32_t y2, int color);

/**
 * Method to draw a circle
 * @param x x coordonate of the center of the circle
 * @param y y coordonate of the center of the circle
 * @param r length of the radius of the circle
 * @param fill_color color of the inside of the circle
 * @param back_color color of the background
 */
extern void lcd_draw_circle(
    uint32_t x, uint32_t y, int r, int fill_color, int back_color);

/**
 * Method to get the current background color used
 * @return value of background color
 */
extern int lcd_get_background_color();

/**
 * Method to remove a char
 * @param x: x coordinate of the upper left char
 * @param y: y coordinate of the upper left char
 */
extern void lcd_remove_char(uint32_t x, uint32_t y);

extern void lcd_remove_string(uint32_t x, uint32_t y, char* str);

extern void lcd_draw_image(Image* image, int x, int y);

#endif /* LCD_H_ */
