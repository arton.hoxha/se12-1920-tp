/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Abstract:    Introduction to C language and modular programming
 *
 * Purpose:     Gives the state and the direction the wheel is turning.
 *
 * Author:      <Arton Hoxha & Corentin Bompard>
 *
 * Date:        <31.03.2020>
 */

#include "wheel.h"

#include <bbb/am335x_gpio.h>
#include <stdio.h>

#include "../gpio/gpio.h"

// pin constant definitions
#define GPIO_B AM335X_GPIO1
#define GPIO_A AM335X_GPIO2
#define CHA 1
#define CHB 29

static struct listener {
    wheel_handler_t routine;
    void* param;
} listener;

static void wheel_handler(void* param)
{
    (void)param;
    enum wheel_direction dir = wheel_get_direction();
    if (listener.routine != 0) {
        listener.routine(dir, listener.param);
    }
}

// initialize modules and pins
void wheel_init()
{
    am335x_gpio_setup_pin_in(GPIO_B, CHB, AM335X_GPIO_PULL_NONE, true);
    am335x_gpio_setup_pin_in(GPIO_A, CHA, AM335X_GPIO_PULL_NONE, true);

    gpio_on_event(GPIO_A,
                  CHA,
                  GPIO_FALLING | GPIO_RISING | GPIO_DEBOUNCED,
                  wheel_handler,
                  0);
}

// returns the direction
enum wheel_direction wheel_get_direction()
{
    enum wheel_direction dir = RIGHT;
    bool cha                 = am335x_gpio_get_state(GPIO_A, CHA);
    bool chb                 = am335x_gpio_get_state(GPIO_B, CHB);
    if (cha == chb) dir = LEFT;  // if same flank then left else right
    return dir;
}

int wheel_on_event(wheel_handler_t routine, void* param)
{
    if (routine == 0) {
        listener.param   = 0;
        listener.routine = 0;
        return 0;
    }

    if (listener.routine == 0) {
        listener.routine = routine;
        listener.param   = param;
        return 0;
    }
    return -1;
}
