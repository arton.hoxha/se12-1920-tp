#pragma once
#ifndef WHEEL_H
#define WHEEL_H
/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Abstract:    Introduction to C language and modular programming
 *
 * Purpose:     Gives the state and the direction the wheel is turning.
 *
 * Author:      <Arton Hoxha & Valentin Antille>
 *
 * Date:        <01.10.2019>
 */

// enum directions for better readability
enum wheel_direction { STILL, RIGHT, LEFT };

/**
 * Handles the rotation of the wheel with interruption
 */
typedef void (*wheel_handler_t)(enum wheel_direction direction, void* param);

/**
 * method to initialize the resoures of the wheel
 * this method shall be called prior any other.
 */
extern void wheel_init();

/**
 * method to hook and unhook a wheel service routine to a specified
 * interrupt source
 *
 * @param routine wheel service routine to hook to the specified
 *                interrupt source, use 0 to unhook the method
 * @param param parameter to be passed as argument while calling the
 *              specified interrupt service routine
 * @return execution status, 0 if success, -1 if already attached
 */
extern int wheel_on_event(wheel_handler_t routine, void* param);

/**
 * returns the direction
 */
extern enum wheel_direction wheel_get_direction();

#endif
