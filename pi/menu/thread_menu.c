#include "thread_menu.h"

#include <stdio.h>

#include "../game/game.h"
#include "../gpio/gpio.h"
#include "../kernel/thread.h"
#include "../lcd/lcd.h"
#include "../wheel/wheel.h"

#define END_OR_RESTART 0
#define PLAY_OR_PLAY_AGAIN 1
#define CHOICE_X 65
#define OFFSET_Y 30
#define REPLAY_X 48
#define REPLAY_Y 20
#define SELECTED_WORD_X 45
#define CLEAR_LCD_POS_X 50
#define CLEAR_LCD_POS_Y 95
#define POS_0 0

// Menu entries
char* choices[2] = {
    [0] = "Yes",
    [1] = "No",
};

// players str
char* players_str[2] = {
    [0] = "Player 1 wins",
    [1] = "Player 2 wins",
};

char* confirm = "Confirm ?";

char* replay = "Replay ?";

// Current id of the menu item selected
unsigned current_choice = PLAY_OR_PLAY_AGAIN;

static void menu_display(game* game)
{
    if (game->inMenu) {
        lcd_draw_rect(POS_0,
                      CLEAR_LCD_POS_X,
                      POS_0,
                      CLEAR_LCD_POS_Y,
                      BLACK);  // clear lcd display
        lcd_display_string(confirm, POS_0, POS_0, false);
        lcd_display_string(game->selected_word, SELECTED_WORD_X, POS_0, false);
        lcd_change_text_color(RED);
        lcd_display_string(choices[0], CHOICE_X, CHOICE_X - OFFSET_Y, false);
        lcd_change_text_color(WHITE);
        lcd_display_string(choices[1], CHOICE_X, CHOICE_X, false);
    } else if (game->inEndMenu) {
        if (game->hasWon) {
            lcd_display_string(players_str[1], POS_0, POS_0, false);
        } else {
            lcd_display_string(players_str[0], POS_0, POS_0, false);
        }
        lcd_display_string(replay, REPLAY_X, REPLAY_Y, false);
        lcd_change_text_color(RED);
        lcd_display_string(choices[0], CHOICE_X, CHOICE_X - OFFSET_Y, false);
        lcd_change_text_color(WHITE);
        lcd_display_string(choices[1], CHOICE_X, CHOICE_X, false);
    }
}

static void change_option(game* game)
{
    if (current_choice == PLAY_OR_PLAY_AGAIN) {
        if (game->inMenu || game->inEndMenu) {
            lcd_change_text_color(RED);
            lcd_display_string(
                choices[0], CHOICE_X, CHOICE_X - OFFSET_Y, false);
            lcd_change_text_color(WHITE);
            lcd_display_string(choices[1], CHOICE_X, CHOICE_X, false);
        }

    } else {
        if (game->inMenu || game->inEndMenu) {
            lcd_change_text_color(RED);
            lcd_display_string(choices[1], CHOICE_X, CHOICE_X, false);
            lcd_change_text_color(WHITE);
            lcd_display_string(
                choices[0], CHOICE_X, CHOICE_X - OFFSET_Y, false);
        }
    }
}

static void menu_wheel_handler(game_menu menu_event, game* game)
{
    if (menu_event == MENU_RENDER) {
        menu_display(game);
    } else if (menu_event == MENU_RESET) {
        current_choice = PLAY_OR_PLAY_AGAIN;
    } else if (menu_event == MENU_LEFT) {
        current_choice = PLAY_OR_PLAY_AGAIN;
        change_option(game);

    } else if (menu_event == MENU_RIGHT) {
        current_choice = END_OR_RESTART;
        change_option(game);
    } else if (menu_event == MENU_CLICK) {
        if (current_choice == END_OR_RESTART) {
            if (game->inMenu) {  // reset
                game->inMenu = false;
                msgq_post(game->events_msq, (void*)GAME_RESET_SELECTION);
            } else if (game->inEndMenu) {  // end game
                msgq_post(game->render_msq, (void*)RENDER_END);
            }

        } else if (current_choice == PLAY_OR_PLAY_AGAIN) {
            if (game->inMenu) {  // play
                msgq_post(game->events_msq, (void*)GAME_START);
                game->inMenu = false;
                game->inGame = true;
            } else if (game->inEndMenu) {  // play again
                game->inEndMenu = false;
                msgq_post(game->events_msq, (void*)GAME_RESET_SELECTION);
            }
        }
        current_choice = PLAY_OR_PLAY_AGAIN;
    }
}

void thread_menu(void* param)
{
    game* game = (struct game*)param;
    while (1) {
        if (game->inMenu || game->inEndMenu) {
            int msg = (int)msgq_fetch(game->menu_msq);
            menu_wheel_handler(msg, game);
        } else {
            thread_yield();
        }
    }
}
