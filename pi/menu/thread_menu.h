#pragma once
#ifndef MENU_H_
#define MENU_H_
/**
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This module is based on the software library developped by Texas Instruments
 * Incorporated - http://www.ti.com/ for its AM335x starter kit.
 *
 * Project:	HEIA-FR / Embedded Systems 2 EP
 *
 * Abstract:  Embedded Project in embedded systems course
 *
 * Purpose:  Thread who manages menus for word selection and play again
 *
 * Author: 	<Corentin Bompard & Lucas Bueche>
 * Date: 	<15.05.2020>
 */

/**
 * Thread which uses game struct to manage menus for word selection and play
 * again
 */
void thread_menu(void* param);

#endif /* MENU_H_ */
