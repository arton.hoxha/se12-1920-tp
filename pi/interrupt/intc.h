#pragma once
#ifndef INTC_H
#define INTC_H

/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems Integrated Project
 * Interruption Purpose:     Control material interruption Author:
 * Bompard Corentin & Hoxha Arton Date: 		25.03.2020
 */

#include "stdbool.h"

// structure of the INTC registries
enum intc_vectors {
    INTC_GPIO2A = 32,
    INTC_GPIO2B,

    INTC_GPIO3A = 62,
    INTC_GPIO3B,

    INTC_TIMER0 = 66,
    INTC_TIMER1,
    INTC_TIMER2,
    INTC_TIMER3,

    INTC_TIMER4 = 92,
    INTC_TIMER5,
    INTC_TIMER6,
    INTC_TIMER7,
    INTC_GPIO0A,
    INTC_GPIO0B,
    INTC_GPIO1A,
    INTC_GPIO1B,
};

typedef void (*intc_service_routine_t)(enum intc_vectors vector_nr,
                                       void* param);

/**
 * initialization method of the intc
 * must be called before any other method in this module
 */
extern void intc_init();

/**
 * method for hooking and unhooking an application-specific
 * interrupt service routine on a parameter given vector
 *
 * @param vector_nr source for the interrupt vector number
 * @param routine interrupt the service routine to associate with the specific
 *                interrupt source, use 0 to unhook the method
 * @param param parameter to be given as argument when calling the
 *              specified service interruption routine
 *
 * @return the execution status: 0 = successful, -1 = error
 */
extern int intc_on_event(enum intc_vectors vector_nr,
                         intc_service_routine_t routine,
                         void* param);

/**
 * function to force/simulate an interrupt request
 *
 * @param vector_nr interrupt vector number for which the interrupt should
 *                   be forced/simulated
 * @param force  true to force the interrupt request, false otherwise
 */
extern void intc_force(enum intc_vectors vector_nr, bool force);

#endif
