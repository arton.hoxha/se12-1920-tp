#include <stdio.h>

#include "buttons.h"
#include "counter.h"
#include "exception.h"
#include "gpio.h"
#include "intc.h"
#include "interrupt.h"
#include "leds.h"
#include "seg7.h"
#include "timer.h"
#include "wheel.h"

#define SW_GPIO AM335X_GPIO1
#define S1_PIN 15
#define S2_PIN 16
#define S3_PIN 17
#define PERIOD 10

static enum dmtimer_timers timer = DMTIMER_2;

struct state {
    int counter;
    bool isCounter;
};

static void s1_handler(void* param)
{
    struct state* states = (struct state*)param;
    leds_set_state(LED_1, true);
    leds_set_state(LED_2, false);
    states->counter   = 0;
    states->isCounter = true;
    counter_display(states->counter);
}

static void s2_handler(void* param)
{
    struct state* states = (struct state*)param;
    leds_set_state(LED_2, true);
    leds_set_state(LED_1, false);
    states->isCounter = false;
}

static void s3_handler(void* param)
{
    struct state* states = (struct state*)param;

    if (states->isCounter) {
        states->counter = 0;
        counter_display(states->counter);
    }
}

// increments/decrements counter
static void wheel_handler(enum wheel_direction dir, void* param)
{
    struct state* states = (struct state*) param;
    // update counter
    if (dir == LEFT && states->counter > -99) states->counter--;
    if (dir == RIGHT && states->counter < 99) states->counter++;

    counter_display(states->counter);
    printf("counter: %d\n", states->counter);
}

void refresh_display(void* param){
    (void) param;
    seg7_refresh_display();
}

void init()
{
    wheel_init();
    seg7_init();
    intc_init();
    interrupt_init();
    exception_init();
    gpio_init();
    leds_init();
}

int main()
{
    struct state states = {0,false};

    init();


    dmtimer_reset(timer);
    dmtimer_on_event(timer, refresh_display,0);
    dmtimer_set_period(timer, PERIOD);
    dmtimer_start(timer);

    gpio_on_event(SW_GPIO, S1_PIN, GPIO_FALLING, s1_handler, &states);
    gpio_on_event(SW_GPIO, S2_PIN, GPIO_FALLING, s2_handler, &states);
    gpio_on_event(SW_GPIO, S3_PIN, GPIO_FALLING, s3_handler, &states);

    wheel_on_event(wheel_handler, &states);

    interrupt_enable(INTERRUPT_ALL);

    while (1)
        ;
}
