/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems Integrated Project
 * Interruption Purpose:     Control material interruption Author:
 * Bompard Corentin & Hoxha Arton Date: 		25.03.2020
 */

#include "intc.h"

#include <stdint.h>
#include <stdio.h>

#include "interrupt.h"
#include "stdbool.h"

#define INTC_NB_OF_VECTORS 128
#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

static struct listener {
    intc_service_routine_t routine;
    void* param;
} listeners[INTC_NB_OF_VECTORS];

static volatile struct intc_ctrl {
    uint32_t revision;
    uint32_t res1[3];
    uint32_t sysconfig;
    uint32_t sysstatus;
    uint32_t res2[10];
    uint32_t sir_irq;
    uint32_t sir_fiq;
    uint32_t control;
    uint32_t protection;
    uint32_t idle;
    uint32_t res3[3];
    uint32_t irq_priority;
    uint32_t fiq_priority;
    uint32_t threshold;
    uint32_t res4[5];
    struct {
        uint32_t itr;
        uint32_t mir;
        uint32_t mir_clear;
        uint32_t mir_set;
        uint32_t isr_set;
        uint32_t isr_clear;
        uint32_t pending_irq;
        uint32_t pending_fiq;
    } flags[4];
    uint32_t ilr[128];
}* intc = (struct intc_ctrl*)0x48200000;

static void interrupt_handler(enum interrupt_vectors i_vector_nr, void* param)
{
    (void)param;
    (void)i_vector_nr;

    // Get which IRQ interruption has been detected (sir_irq)
    uint32_t vector_nr = intc->sir_irq & 0x7f;

    if (listeners[vector_nr].routine != 0) {
        listeners[vector_nr].routine(vector_nr, listeners[vector_nr].param);
    } else {
        // There's an unknow interruption
        intc->flags[vector_nr / 32].mir_set = 1 << (vector_nr % 32);
    }

    // Quit the interruption mode
    intc->control = 0x1;
}

void intc_init()
{
    interrupt_on_event(INT_IRQ, interrupt_handler, 0);

    // Set the default values
    // no threshold or no nested interrupt
    intc->sysconfig = 0x0;
    intc->idle      = 0x0;
    intc->threshold = 0xff;

    // Reset the controler intc
    intc->sysconfig = 0x2;
    while ((intc->sysstatus & 0x1) == 0)
        ;

    // Hide all interrupt lines
    for (int i = ARRAY_SIZE(intc->flags) - 1; i >= 0; i--) {
        intc->flags[i].mir       = -1;
        intc->flags[i].mir_set   = -1;
        intc->flags[i].isr_clear = -1;
    }

    // Set the INTC controller to generate IRQs
    for (int i = ARRAY_SIZE(intc->ilr) - 1; i >= 0; i--) {
        intc->ilr[i] = 0;
    }
}

int intc_on_event(enum intc_vectors vector_nr,
                  intc_service_routine_t routine,
                  void* param)
{
    if (vector_nr > INTC_NB_OF_VECTORS) return -1;
    struct listener* listener = &listeners[vector_nr];

    if (routine == 0) {
        // Stops interruptions from this source
        intc->flags[vector_nr / 32].mir_set = 1 << (vector_nr % 32);
        listener->routine                   = 0;
        listener->param                     = 0;
        return 0;
    }
    if (listener->routine == 0) {
        listener->routine = routine;
        listener->param   = param;
        // Allows interruptions from this source
        intc->flags[vector_nr / 32].mir_clear = 1 << (vector_nr % 32);
        return 0;
    }

    return -1;
}

void intc_force(enum intc_vectors vector_nr, bool force)
{
    // Defensive approach, we allow vector numbers in the range of vects
    if (vector_nr < INTC_NB_OF_VECTORS) {
        if (force) {
            // Simulation of interruptions from vector_nr
            intc->flags[vector_nr / 32].isr_set = 1 << (vector_nr % 32);
        } else {
            // Stop of the simulation from vector_nr
            intc->flags[vector_nr / 32].isr_clear = 1 << (vector_nr % 32);
        }
    }
}
