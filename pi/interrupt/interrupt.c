/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems Integrated Project
 * Interruption
 *
 * Author: 		Bompard Corentin & Hoxha Arton
 * Date: 		18.03.2020
 */

#include "interrupt.h"

#include <stdio.h>

static struct listener {
    interrupt_service_routine_t routine;
    void* param;
} listeners[INT_NB_VECTORS];

extern void interrupt_handler(enum interrupt_vectors vector_nr)
{
    struct listener* listener = &listeners[vector_nr];
    if (listener->routine != 0) {
        listener->routine(vector_nr, listener->param);
    } else {
        printf(
            "No interrupt service routine hooked to the vector# %d. "
            "Freezing!\n",
            vector_nr);
        while (1)
            ;
    }
}

/**
  Interrupt logic low level initialization routine.
  This method is implemented in assembler file interrupt_asm.S
 */
extern void interrupt_asm_init();

extern void interrupt_init() { interrupt_asm_init(); }

extern int interrupt_on_event(enum interrupt_vectors vector_nr,
                              interrupt_service_routine_t routine,
                              void* param)
{
    struct listener* listener = &listeners[vector_nr];
    if (listener->routine == 0) {
        listener->routine = routine;
        listener->param   = param;
    } else if (routine == 0) {
        listener->routine = 0;
    } else {
        return 1;
    }
    return 0;
}
