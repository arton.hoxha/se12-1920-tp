#include "game.h"

#include "../counter/counter.h"
#include "../kernel/thread.h"
#include "../lcd/lcd.h"
#include "stdio.h"
#include "string.h"

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))
#define WORD_LENGTH 9
#define ERROR 1
#define NO_ERROR 0
#define TIME_FACTOR_DIFFICULTY 5
#define MAX_NB_ERRORS 6

char alphabet[26] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
                     'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
                     's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

void game_init(struct game* game)
{
    game->menu_msq             = msgq_create(32, true);
    game->render_msq           = msgq_create(32, true);
    game->events_msq           = msgq_create(32, true);
    game->counter              = 0;
    game->errors               = 0;
    game->inMainScreen         = true;
    game->inSelection          = false;
    game->inMenu               = false;
    game->inGame               = false;
    game->inEndMenu            = false;
    game->hasGameEnded         = false;
    game->hasWon               = false;
    game->selected_char_index  = 0;
    game->correct_letters      = 0;
    game->selected_char        = 'a';
    game->selected_word        = "         ";  // init 9 chars word
    game->selected_word_length = 0;
    for (unsigned i = 0; i < WORD_LENGTH; i++) {
        game->are_chars_in_word[i] = 0;
    }
    msgq_post(game->render_msq, (void*)RENDER_MAIN_MENU);
}

// custom str_length because C does not differenciante empty char with char
static int str_length(char* str)
{
    int len = 0;
    for (int i = 0; i < WORD_LENGTH - 1; i++) {
        if (str[i] == ' ' || str[i] == '\0') {
            break;
        }
        len++;
    }
    return len;
}

static void check_word(game* game)
{
    int detected_error = ERROR;

    for (int i = 0; i < game->selected_word_length; i++) {
        if (game->selected_char == game->selected_word[i]) {
            if (!game->are_chars_in_word[i]) {
                game->correct_letters++;
                game->are_chars_in_word[i] = 1;
                msgq_post(game->render_msq, (void*)RENDER_WORD_CHAR);
                detected_error = NO_ERROR;
                game->counter =
                    (game->selected_word_length - game->correct_letters + 1) *
                    TIME_FACTOR_DIFFICULTY;
            }
        }
    }
    if (game->correct_letters == game->selected_word_length) {
        game->hasWon = true;
        msgq_post(game->events_msq, (void*)GAME_STOP);
    } else if (detected_error) {
        game->errors = game->errors + 1;
        if (game->errors == MAX_NB_ERRORS) {
            msgq_post(game->events_msq, (void*)GAME_STOP);
        } else {
            msgq_post(game->render_msq, (void*)RENDER_NEXT_PART);
        }
    }
}

void handle_events(game_events event, game* game)
{
    static int index = 0;

    if (event == GAME_END) {
        game->inEndMenu    = false;
        game->inGame       = false;
        game->inMenu       = false;
        game->inSelection  = false;
        game->inMainScreen = false;
        game->hasGameEnded = true;
    } else if (event == GAME_STOP) {
        game->inEndMenu   = true;
        game->inGame      = false;
        game->inMenu      = false;
        game->inSelection = false;
        msgq_post(game->render_msq, (void*)RENDER_END_MENU);
    }
    if (event == GAME_WHEEL_RIGHT) {
        if (game->inGame) {
            // select next right char
            game->selected_char_index =
                (game->selected_char_index + 1) % ARRAY_SIZE(alphabet);
            game->selected_char = alphabet[game->selected_char_index];
            msgq_post(game->render_msq, (void*)RENDER_CHAR);

        } else if (game->inSelection) {
            // select next right char
            game->selected_char_index =
                (game->selected_char_index + 1) % ARRAY_SIZE(alphabet);
            game->selected_char = alphabet[game->selected_char_index];
            msgq_post(game->render_msq, (void*)RENDER_CHAR);
        } else if (game->inMenu || game->inEndMenu) {
            // select 'no'
            msgq_post(game->menu_msq, (void*)MENU_RIGHT);
        }

    } else if (event == GAME_WHEEL_LEFT) {
        if (game->inGame) {
            // select next left char
            game->selected_char_index = (game->selected_char_index - 1) < 0
                                            ? game->selected_char_index =
                                                  ARRAY_SIZE(alphabet) - 1
                                            : game->selected_char_index - 1;
            game->selected_char = alphabet[game->selected_char_index];
            msgq_post(game->render_msq, (void*)RENDER_CHAR);

        } else if (game->inSelection) {
            // select next left char
            game->selected_char_index = (game->selected_char_index - 1) < 0
                                            ? game->selected_char_index =
                                                  ARRAY_SIZE(alphabet) - 1
                                            : game->selected_char_index - 1;
            game->selected_char = alphabet[game->selected_char_index];
            msgq_post(game->render_msq, (void*)RENDER_CHAR);
        } else if (game->inMenu || game->inEndMenu) {
            // select 'yes'
            msgq_post(game->menu_msq, (void*)MENU_LEFT);
        }

    } else if (event == GAME_WHEEL_CLICK) {
        if (game->inSelection) {
            // validate selected char in selection
            if (index != WORD_LENGTH - 1) {
                game->selected_word[index++] = game->selected_char;
                msgq_post(game->render_msq, (void*)RENDER_CHAR);
                msgq_post(game->render_msq, (void*)RENDER_NEXT_CHAR);
            } else {
                // all chars are entered
                game->inSelection            = false;
                game->inMenu                 = true;
                game->selected_word[index++] = game->selected_char;
                game->selected_word_length   = strlen(game->selected_word);
                game->counter =
                    (strlen(game->selected_word) + 1) * TIME_FACTOR_DIFFICULTY;
                game->selected_char       = 'a';
                game->selected_char_index = 0;
                msgq_post(game->menu_msq, (void*)MENU_RENDER);
            }
        } else if (game->inMainScreen) {
            // go to selection screen
            game->inMainScreen = false;
            game->inSelection  = true;
            msgq_post(game->render_msq, (void*)RENDER_SELECTION);
        } else if (game->inGame) {
            // player 2 is playing and select char
            check_word(game);
        } else if (game->inMenu || game->inEndMenu) {
            // menu click in selection word confirm or in end menu
            msgq_post(game->menu_msq, (void*)MENU_CLICK);
        }
    } else if (event == GAME_START) {
        msgq_post(game->render_msq, (void*)RENDER_GAME);
        counter_display(game->counter);

    } else if (event == GAME_RESET_SELECTION) {
        // https://stackoverflow.com/questions/53943743/how-to-reset-a-string-in-c
        game->selected_word =
            memset(game->selected_word, 0, WORD_LENGTH);  // set all 0
        index                      = 0;
        game->hasGameEnded         = false;
        game->inEndMenu            = false;
        game->inGame               = false;
        game->inMainScreen         = false;
        game->inMenu               = false;
        game->inSelection          = true;
        game->hasWon               = false;
        game->errors               = 0;
        game->selected_char_index  = 0;
        game->selected_char        = 'a';
        game->selected_word_length = 0;
        game->correct_letters      = 0;
        for (int i = 0; i < WORD_LENGTH; i++) {
            game->are_chars_in_word[i] = 0;
        }
        msgq_post(game->render_msq, (void*)RENDER_RESET);
    } else if (event == GAME_BLINK_ON) {
        // display char
        if (!game->inMenu) {
            msgq_post(game->render_msq, (void*)RENDER_CHAR);
        }

    } else if (event == GAME_BLINK_OFF) {
        // remove char
        if (!game->inMenu) {
            msgq_post(game->render_msq, (void*)RENDER_REMOVE_CHAR);
        }
    } else if (event == GAME_BTN_CLICK) {
        if (game->inSelection) {
            // select word without entering 9 chars
            game->inSelection          = false;
            game->inMenu               = true;
            game->selected_word_length = str_length(game->selected_word);
            game->selected_char        = 'a';
            game->selected_char_index  = 0;
            game->counter =
                (str_length(game->selected_word) + 1) * TIME_FACTOR_DIFFICULTY;
            msgq_post(game->menu_msq, (void*)MENU_RENDER);
        }
    }
}

void thread_game(void* param)
{
    game* game = (struct game*)param;
    while (true) {
        handle_events((int)msgq_fetch(game->events_msq), game);
    }
}
