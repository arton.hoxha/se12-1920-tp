#pragma once
#ifndef MAIN_MENU_H
#define MAIN_MENU_H

/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 2 EP
 *
 * Abstract:    Embedded Project in embedded systems course
 *
 * Purpose:     Image for the welcome screen
 *
 * Author:      <Corentin Bompard & Arton Hoxha>
 *
 * Date:        <15.05.2020>
 */

#include "../lcd/lcd.h"

/**
 * Image shown at the welcome screen
 */
extern Image main_menu;
#endif
