#pragma once
#ifndef GAME_H
#define GAME_H
/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Abstract:    Introduction to C language and modular programming
 *
 * Purpose:     Gives the state and the direction the wheel is turning.
 *
 * Author:      <Corentin Bompard & Arton Hoxha>
 *
 * Date:        <01.10.2019>
 */

#include "../kernel/msgq.h"
#include "stdint.h"

// game structure for our entire game
typedef struct game {
    msgqid_t menu_msq;
    msgqid_t render_msq;
    msgqid_t events_msq;
    bool inMenu;
    bool inMainScreen;
    bool inSelection;
    bool inGame;
    bool inEndMenu;
    bool hasGameEnded;
    bool hasWon;
    char selected_char;
    char* selected_word;
    uint8_t errors;
    uint8_t selected_char_index;
    int counter;
    int selected_word_length;
    int correct_letters;
    int are_chars_in_word[];
} game;

// enumeration for all events in our game
typedef enum {
    GAME_BTN_CLICK,
    GAME_WHEEL_LEFT,
    GAME_WHEEL_RIGHT,
    GAME_WHEEL_CLICK,
    GAME_BLINK_ON,
    GAME_BLINK_OFF,
    GAME_START,
    GAME_STOP,
    GAME_END,
    GAME_RESET_SELECTION
} game_events;

// enumeration for all events in menu
typedef enum {
    MENU_RESET,
    MENU_RIGHT,
    MENU_LEFT,
    MENU_CLICK,
    MENU_RENDER,
} game_menu;

// enumeration for render in lcd screen
typedef enum {
    RENDER_MAIN_MENU,
    RENDER_GAME,
    RENDER_END_MENU,
    RENDER_SELECTION,
    RENDER_RESET,
    RENDER_CHAR,
    RENDER_WORD_CHAR,
    RENDER_NEXT_CHAR,
    RENDER_REMOVE_CHAR,
    RENDER_NEXT_PART,
    RENDER_END
} game_render;

/**
 * Thread which receives game struct pointer and manage variables
 * @param param: expect to receive game struct
 */
void thread_game(void* param);

/**
 * init the game strutct
 */
void game_init();

#endif
