/**
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: HEIA-FR / Embedded Systems 2
 *
 * Abstract: RFid Demo Program
 *
 * Purpose: This module implements a basic RFid tag reader test based on
 *          the CR95HF device.
 *
 * Author:  Daniel Gachet / HEIA-FR
 * Date:    05.04.2019
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <bbb/am335x_dmtimer1.h>
#include "rfid.h"

int main()
{
    printf("\nRFid - test program...\n");

    init_cpu_and_peripherals();
    printf("CR95HF Device Connected.\n");
    printf("CR95HF Device ID: %s\n", rfid_read_chip_id());

    printf("Put the RFid Tag over the RFid Click Antenna\n");
    while (1) {
        char id[RFID_ID_LEN] = { [0]=0, };
        int err = rfid_get_tag_id(id);
        if (err == 0) {
            printf("Tag ID : %s\n", id);
            am335x_dmtimer1_wait_ms(200);
        }
    }
    return 0;
}
