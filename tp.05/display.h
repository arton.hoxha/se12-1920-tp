#pragma once
#ifndef DISPLAY_H
#define DISPLAY_H

/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 3 Laboratory
 *
 * Abstract:    Peripheral devices implementation with C
 *
 * Purpose:     Displays different values in the oled display
 *
 * Author:      <Arton Hoxha, Samuel Fringeli and Léonard Noth>
 *
 * Date:        <24.12.2019>
 */

#include <stdbool.h>
#include <stdint.h>
#define BLUE 0x0010
#define BLUE_SKY 0x041F
#define RED 0xf0a4
#define GREEN 0x07E0
#define BLACK 0
#define WHITE 0xFFFF
#define SCREEN_WIDTH 96
#define SCREEN_HEIGHT 96

// struct to use (x,y) coordinates
typedef struct {
    uint32_t x;
    uint32_t y;
} display_point;

/**
 * initialize the display
 */
void display_init();

/**
 * Displays a rectangle
 *
 * (x,y)
 *  .-----------+
 *  |           |
 *  |           |
 *  +-----------+
 *
 *
 * @param position the (x,y) position, top left of the rectangle
 * @param width width of the rectangle
 * @param length length of the rectangle
 * @param color color of the rectangle
 */
void display_rectangle(display_point position,
                       int width,
                       int length,
                       uint32_t color);

/**
 * Displays a char
 *
 *        0   1   2   3   4   5   6   7
 * (x,y).---+---+---+---+---+---+---+---+
 *  |   | 0 | 0 | 1 | 1 | 1 | 0 | 0 | 0 |
 *  |   +---+---+---+---+---+---+---+---+
 *  |   | 0 | 1 | 1 | 0 | 1 | 1 | 0 | 0 |
 * \ /  +---+---+---+---+---+---+---+---+
 *      | 1 | 1 | 0 | 0 | 0 | 1 | 1 | 0 |
 *      +---+---+---+---+---+---+---+---+
 *      | 1 | 1 | 0 | 0 | 0 | 1 | 1 | 0 |
 *      +---+---+---+---+---+---+---+---+
 *      | 1 | 1 | 1 | 1 | 1 | 1 | 1 | 0 |
 *      +---+---+---+---+---+---+---+---+
 *      | 1 | 1 | 0 | 0 | 0 | 1 | 1 | 0 |
 *      +---+---+---+---+---+---+---+---+
 *      | 1 | 1 | 0 | 0 | 0 | 1 | 1 | 0 |
 *      +---+---+---+---+---+---+---+---+
 *      | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
 *      +---+---+---+---+---+---+---+---+
 *  This is the char 'A' from  the font8x8.h file
 *  The logic is the following, the position is in
 *  the upper left of the char. To render in the correct
 *  order,we begin at the top left and follow the direction
 *  of the arrow. So we render the first column(0) and continue
 *  with the other columns then.
 *
 * @param c char to display
 * @param position upper left (x,y) position of the char
*/
void display_char(char c, display_point position, uint32_t color);

/**
 * Displays a string
 *
 * The logic is the same as the char we simply render a sequence of characters.
 *
 * @param string the string to display
 * @param position upper left (x,y) position of the string
 *
*/
extern void display_string(char* string, display_point position, uint32_t color);

/**
 *
 * Displays a rectangle in another way than display_rectangle(better for assembly)
 *
 * @param x x coord
 * @param y y coord
 * @param width width of the rectangle
 * @param height height of the rectangle
*/
extern void new_display_rectangle(uint32_t x, uint32_t y, int width, uint32_t height);

#endif
