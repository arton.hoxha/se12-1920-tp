# TP.05 : Introduction au langage assembleur et interface avec le C

## Objectifs

A la fin du laboratoire, les étudiant-e-s seront capables de

* Concevoir et réaliser un programme en langage assembleur
* Interfacer des fonctions assembleur avec des fonctions en C
* Débugger un programme mixte assembleur et C
* Intégrer des composants développés lors de travaux précédents

Durée du travail pratique

* 1 séance de laboratoire (4 heures) + travail personnel

Rapport à rendre

* un journal de laboratoire avec le code source sur le dépôt centralisé

## Travail à réaliser

Ce TP a pour objectif la conception et réalisation en langage assembleur
d'une figure [fractale](https://fr.wikipedia.org/wiki/Fractale) sur l'écran 
LCD OLED de notre cible Beaglebone.

Nous allons choisir les ensembles de 
[Cantor](https://fr.wikipedia.org/wiki/Ensemble_de_Cantor).

![Cantor](img/cantor.jpg)

Afin de rendre le visuel plus intéressant, nous proposons de remplacer 
les lignes par une notre propre figure, un H. 

![Cantor-H](img/cantor-h.jpg)

Comme on peut le constater sur la figure ci-dessous, la taille des barres 
verticales du H aura la moitié de la longueur de la ligne horizontale.

![Cantor-H-line](img/cantor-h-line.png)

Finalement, le résultat du projet devra permettre d'afficher les 2 figures
sur l'écran LCD OLED-C.

![Final-Version](img/final.jpg)


## Conditions

* Rendu
  * Le code et le rapport seront rendus au travers du dépôt Git centralisé
    * sources: _.../tp/tp.05_
    * rapport: _.../tp/tp.05/doc/report.pdf_

* Délai
  * Le journal et le code doivent être rendus au plus tard le soir même du TP à 23h59
