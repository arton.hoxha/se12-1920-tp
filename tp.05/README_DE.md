# TP.05 : Einführung in die Assemblersprache und Schnittstelle mit C

## Ziele

Am Ende des Labors sind die Studenten/Studentinnen fähig,

* Ein Programm in Assemblersprache zu entwerfen und zu realisieren
* Assemblerfunktionen mit C-Funktionen zu verbinden
* Ein aus Assembler und C gemischtes Programm zu debuggen
* entwickelte Komponenten aus früheren Arbeiten zu integrieren

Dauer der praktischen Arbeit

* 1 Laboreinheiten (4 Stunden) + persönliche Arbeit

Abzugeben:

* Laborbericht und den Sourcecode auf das zentralisierte Depot


## Aufgaben

Das Ziel dieses TP ist die Konzeption und Entwicklung einer
[fraktalen](https://fr.wikipedia.org/wiki/Fractale) Figur auf dem LCD OLED
Display des Beaglebone in Assemblersprache.

Wir werden die [Cantor](https://fr.wikipedia.org/wiki/Ensemble_de_Cantor)-Sets auswählen.

![Cantor](img/cantor.jpg)

Um das Visuelle interessanter zu machen, schlagen wir vor, die Linien durch
unsere eigene Figur, nämlich ein "H", zu ersetzen. 

![Cantor-H](img/cantor-h.jpg)

Wie in der Abbildung unten zu sehen ist, beträgt die Größe der vertikalen
Balken des H die Hälfte der Länge der horizontalen Linie.

![Cantor-H-line](img/cantor-h-line.png)

Schliesslich sollte das Ergebnis des Projekts es ermöglichen,
die 2 Figuren auf dem OLED-C LCD-Bildschirm darzustellen.

![Final-Version](img/final.jpg)

## Bedingungen

* Abgabe
    * Der Code und der Bericht werden auf das zentralisierte Git-Depot geladen
        * sources: _.../tp/tp.05_
        * Bericht: _.../tp/tp.05/doc/report.pdf_

* Frist
  * Das Arbeitstagebuch und der Code müssen bis spätestens 23:59 Uhr am Abend des TPs zurückgegeben werden.
