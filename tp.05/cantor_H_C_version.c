#include <stdint.h>
#include "cantor.h"
#include "display.h"


void draw_H(uint32_t x, uint32_t y, uint32_t w, uint32_t h){
    //dessin du H
    new_display_rectangle(x, y, w, h);//barre horizontale
    new_display_rectangle(x, y- w/4, h, w/2);
    new_display_rectangle(x+w, y-w/4, h, w/2);
}


void cantor_H2(uint32_t x, uint32_t y, uint32_t w, uint32_t h){
    if (w == 3) return;

    draw_H(x,y,w,h);

    //H en haut à gauche
    cantor_H2(x - w/4, y + w/4, w/2, h);
    //H en bas à gauche
    cantor_H2(x-w/4, y-w/4, w/2, h);
    //H en haut à droite
    cantor_H2(x+w-w/4, y+w/4, w/2, h);
    //H en bas à droite
    cantor_H2(x+w-w/4, y-w/4, w/2, h);

}
