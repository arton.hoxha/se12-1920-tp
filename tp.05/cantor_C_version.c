#include <stdint.h>
#include "display.h"

void cantor2(uint32_t x, uint32_t y, uint32_t w, uint32_t h){
    if(w == 0)return;
    new_display_rectangle(x,y,w,h);
    //pattern de gauche
    cantor2(x, y+6, w/3, h);

    //pattern de droite
    cantor2(x + w*2/3, y+6, w/3,  h);
}
