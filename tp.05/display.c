/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 3 Laboratory
 *
 * Abstract:    Peripheral devices implementation with C
 *
 * Purpose:     Displays different values in the oled display
 *
 * Author:      <Arton Hoxha, Samuel Fringeli and Léonard Noth>
 *
 * Date:        <24.12.2019>
 */

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "display.h"
#include "bbb/font_8x8.h"
#include <bbb/oled.h>

// constants definition
#define FONT_SIZE 7
#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

// initialize the display
void display_init() {
    oled_init(OLED_V101);
}

// displays a line
void display_line(display_point position, uint32_t size, uint32_t color, bool isHorizontal) {
    if (isHorizontal) {
        oled_memory_size(position.x, position.x, position.y, position.y + size);
    } else {
        oled_memory_size(position.x, position.x + size, position.y, position.y);
    }

    for (uint32_t i = 0; i < size; i++) {
        oled_color(color);
    }
}

// better display_rectangle for assembly
void new_display_rectangle(uint32_t x, uint32_t y, int width, uint32_t height) {
    uint32_t x_dest = (x + width <= SCREEN_WIDTH) ? x + width : SCREEN_WIDTH;
    oled_memory_size(x, x_dest, y, y + height);
    for (uint32_t i = 0; i < (width + 1) * (height + 1); i++) {
        oled_color(RED);
    }
}

// displays a rectangle
void display_rectangle(display_point position, int width, int length, uint32_t color) {
    oled_memory_size(position.x, position.x + length, position.y, position.y + width);
    for (int i = 0; i < (width + 1) * (length + 1); i++) {
        oled_color(color);
    }
}

// displays a char
void display_char(char c, display_point position, uint32_t color) {
    oled_memory_size(position.x, position.x + FONT_SIZE, position.y, position.y + FONT_SIZE);

    for (int i = FONT_SIZE; i >= 0; i--) {
        for (int j = 0; j < FONT_SIZE + 1; j++) {
            if (fontdata_8x8[(int)c][j] & (1 << i)) {
                oled_color(color);
            } else {
                oled_color(BLACK);
            }
        }
    }
}

// displays a string
void display_string(char* word, display_point position, uint32_t color) {
    // i just need the (x;y) coord and i can display a word
    display_point next_char = position;

    for (int i = 0; word[i] != '\0'; i++) {
        if (i == 0) {
            display_char(word[i], position, color);
        } else {
            next_char.y += FONT_SIZE;
            display_char(word[i], next_char, color);
        }
    }
}
