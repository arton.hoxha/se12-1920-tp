/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Abstract:    Introduction to C language and modular programming
 *
 * Purpose:     Control the beaglebone component and display either a serpentine or a counter controled by the encoder
 *
 * Author:      <Arton Hoxha & Valentin Antille>
 *
 * Date:        <01.10.2019>
 */

#include "buttons.h"
#include "leds.h"
#include "seg7.h"
#include "counter.h"
#include"encoder.h"
#include "serpentin.h"

//enum to the three different modes possible
enum modes {COUNTER, SERPENTINE, RESET};

int main()
{
    //initialization
    leds_init();
    buttons_init();
    seg7_init();
    counter_init();
    encoder_init();
    serpentin_init();
    enum modes mode = RESET;

    //running loop
    while (true) {

        //check if the mode changes
        if(buttons_get_state(BTN_1)) mode = COUNTER;
        else if(buttons_get_state(BTN_2)) mode = SERPENTINE;
        else if(buttons_get_state(BTN_3)) mode = RESET;

        //light the leds when the buttons are pressed
        leds_set_state(LED_1, buttons_get_state(BTN_1));
        leds_set_state(LED_2, buttons_get_state(BTN_2));
        leds_set_state(LED_3, buttons_get_state(BTN_3));

        //select between the three modes
        if(mode == RESET){
            serpentin_reset();
            reset_counter();
            mode = COUNTER;//if we reset we go back to counter mode
        }
        else if(mode == COUNTER){
            counter_operations();
        }
        else if(mode == SERPENTINE){
            serpentin_operations();
        }

        refresh_display();
    }

    return 1;
}
