#pragma once
#ifndef COUNTER_H
#define COUNTER_H
/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Abstract:    Introduction to C language and modular programming
 *
 * Purpose:     Counter to compute the value coming from the encoder and send to the 7 segments for display
 *
 * Author:      <Arton Hoxha & Valentin Antille>
 *
 * Date:        <01.10.2019>
 */

/**
 * initialize the counter
 */
void counter_init();

/**
 * displays the value of the counter in the segments
 */
void counter_display(int value);

/**
 * operation to change the counter value according to the direction of the encoder
 */
void counter_operations();

/**
 * resets the counter
 */
void reset_counter();

#endif
