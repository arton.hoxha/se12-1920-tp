/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Abstract:    Introduction to C language and modular programming
 *
 * Purpose:     Gives the state and the direction the encoder is turning.
 *
 * Author:      <Arton Hoxha & Valentin Antille>
 *
 * Date:        <01.10.2019>
 */

#include <bbb/am335x_gpio.h>
#include "encoder.h"

// pin constant definitions
#define GPIO1 AM335X_GPIO1
#define GPIO2 AM335X_GPIO2
#define CHA 1
#define CHB 29

/**
 * The encoder must work the following:
 *         B|A
 *  state: 0|0 = 0
 *  state: 0|1 = 1
 *  state: 1|0 = 2
 *  state: 1|1 = 3
 *
 *         --- 0 ---
 *        |         |
 *        2         1 ==> represents the pysical encoder.
 *        |         |
 *         --- 3 ---
 *
 * Passing from 0 to 1, 1 to 3, 3 to 2 and 2 to 0 means that there is a rotation
 * to the right. From 0 to 2, 2 to 3, 3 to 1 and 1 to 0 is a rotation to the
 * left.
 *
 * The truth table is composed of the former state(fs) and the new state.
 *
 *      L: rotation to the left
 *      R: rotation to the right
 *      S: stay still
 *
 *            new state
 *         | 0 | 1 | 2 | 3 |
 *         +---------------+
 *   f s  0| S | S | S | S |  Normally passing from fs(0) to ns(1) or ns(2) must be going right, left respectively.
 *         +---------------+  Those states are Still because it rotates one time too much. Instead of incrementing by one 1
 *        1| L | S | S | R |  in counter it increments by two so those states remains Still. The same is for
 *         +---------------+  fs(3) to ns(1) or ns(2).
 *        2| R | S | S | L |
 *         +---------------+
 *        3| S | S | S | S |
 *         +---------------+
 *
 */

// enum which corresponds to the truth table
static const enum encoder_direction directions[4][4] = {
    {STILL, STILL, STILL, STILL},
    {LEFT, STILL, STILL, RIGHT},
    {RIGHT, STILL, STILL, LEFT},
    {STILL, STILL, STILL, STILL}};

// former state which holds the value between function calls
static int former_state = 0;

// initialize modules and pins
void encoder_init()
{
    am335x_gpio_init(GPIO1);
    am335x_gpio_init(GPIO2);

    am335x_gpio_setup_pin_in(GPIO1, CHB, AM335X_GPIO_PULL_NONE, true);
    am335x_gpio_setup_pin_in(GPIO2, CHA, AM335X_GPIO_PULL_NONE, true);
}
//get the state of the encoder
int get_encoder_state()
{
    int state = 0;
    if (am335x_gpio_get_state(GPIO2, CHA)) state += 1;
    if (am335x_gpio_get_state(GPIO1, CHB)) state += 2;

    return state;
}

// returns the direction
enum encoder_direction encoder_direction()
{
    int state = get_encoder_state();  // gets the new direction
    enum encoder_direction direction = directions[former_state][state];
    former_state = state;//the new state becomes the former state
    return direction;
}
