#pragma once
#ifndef SERPENTINE_H
#define SERPENTINE_H

/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Abstract:    Introduction to C language and modular programming
 *
 * Purpose:     Make a serpentin navigating through the 7 segments display
 *
 * Author:      <Arton Hoxha & Valentin Antille>
 *
 * Date:        <01.10.2019>
 */


/**
 * method to initialize the serpentin
 */
void serpentin_init();

/**
 * method to move the serpentin around
 */
void serpentin_move();

/**
 * method to reset to the initial state
 */
void serpentin_reset();

/**
 * method to get the wheel information and call serpentin_move() accordingly
 */
void serpentin_operations();


#endif
