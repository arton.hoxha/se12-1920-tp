/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Abstract:    Introduction to C language and modular programming
 *
 * Purpose:     Counter to compute the value coming from the encoder and send to the 7 segments for display
 *
 * Author:      <Arton Hoxha & Valentin Antille>
 *
 * Date:        <01.10.2019>
 */

#include <bbb/am335x_gpio.h>
#include "counter.h"
#include "encoder.h"
#include "seg7.h"

/* structure to initialize gpio pins used by 7-segment
   segment definition

           +-- SEG_A --+
           |           |
         SEG_F       SEG_B
           |           |
           +-- SEG_G --+
           |           |
         SEG_E       SEG_C
           |           |
           +-- SEG_D --+
*/

//static counter which holds value after function calls
static int counter = 0;

//construct for all digits
const uint32_t digits[] = {
    (SEG7_A + SEG7_B + SEG7_C + SEG7_D + SEG7_E + SEG7_F),           // 0
    (SEG7_B + SEG7_C),                                               // 1
    (SEG7_A + SEG7_B + SEG7_D + SEG7_E + SEG7_G),                    // 2
    (SEG7_A + SEG7_B + SEG7_C + SEG7_D + SEG7_G),                    // 3
    (SEG7_B + SEG7_C + SEG7_F + SEG7_G),                             // 4
    (SEG7_A + SEG7_C + SEG7_D + SEG7_F + SEG7_G),                    // 5
    (SEG7_A + SEG7_C + SEG7_D + SEG7_E + SEG7_F + SEG7_G),           // 6
    (SEG7_A + SEG7_B + SEG7_C),                                      // 7
    (SEG7_A + SEG7_B + SEG7_C + SEG7_D + SEG7_E + SEG7_F + SEG7_G),  // 8
    (SEG7_A + SEG7_B + SEG7_C + SEG7_D + SEG7_F + SEG7_G),           // 9
};

//counter init includes init of seg7 and encoder
void counter_init() {
    seg7_init();
    encoder_init();
}

//displays the counter in segments
void counter_display(int value)
{
    bool show_dot = false;//bool to show the dot for negative numbers or not
    if (value < 0) {
        value    = -value;
        show_dot = true;
    }
    display_dots(show_dot);

    // first digit of value is left, right respectively
    display_segments(SEG7_LEFT, digits[(value / 10) % 10]);
    display_segments(SEG7_RIGHT, digits[(value % 10)]);
}

//change counter value according to the direction of the encoder
void counter_operations()
{
    enum encoder_direction direction = encoder_direction();
    if (direction == LEFT) {
        if (counter > -99) {
            counter -= 1;
        } else {
            counter = -99;
        }

    } else if (direction == RIGHT) {
        if (counter < 99) {
            counter += 1;
        } else {
            counter = 99;
        }
    }

    counter_display(counter);
}

//reset counter
void reset_counter() { counter = 0; }
