/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Abstract:    Introduction to C language and modular programming
 *
 * Purpose:     Define the specification to use the 7-segments display
 *
 * Author:      <Arton Hoxha & Valentin Antille>
 * Date:        <01.10.2019>
 */


#include <bbb/am335x_gpio.h>
#include <stdbool.h>
#include <stdio.h>
#include "seg7.h"


// pin definition for 7-segment access
#define GPIO0 AM335X_GPIO0
#define GPIO2 AM335X_GPIO2

// define the two displays
#define DIG1 2
#define DIG2 3

//define the points
#define DP1 4
#define DP2 5

//define the 7 segments
#define SEGA (1 << 4)
#define SEGB (1 << 5)
#define SEGC (1 << 14)
#define SEGD (1 << 22)
#define SEGE (1 << 23)
#define SEGF (1 << 26)
#define SEGG (1 << 27)
#define SEG_ALL (SEGA | SEGB | SEGC | SEGD | SEGE | SEGF | SEGG)

//define the size of the array
#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

//save the pin numbers of each segment to initialize them later
static const struct gpio_init {
    uint32_t pin_nr;
} gpio_init[] = {
    {4},   // SEGA
    {5},   // SEGB
    {14},  // SEGC
    {22},  // SEGD
    {23},  // SEGE
    {26},  // SEGF
    {27},  // SEGG
};

//struct to manipulate both the 7 segments displays
static struct digit {
    uint32_t seg7;
    uint32_t dot;
    uint32_t digit;
} segments[2] = {
    [SEG7_LEFT] =
        {
            .digit = DIG1,
        },
    [SEG7_RIGHT] =
        {
            .digit = DIG2,
        },
};

//initialize and turn off every 7 segments' elements
void seg7_init()
{
    am335x_gpio_init(GPIO0);
    am335x_gpio_init(GPIO2);
    am335x_gpio_setup_pin_out(GPIO2, DIG1, false);
    am335x_gpio_setup_pin_out(GPIO2, DIG2, false);
    am335x_gpio_setup_pin_out(GPIO2, DP1, false);
    am335x_gpio_setup_pin_out(GPIO2, DP2, false);
    for (int i = ARRAY_SIZE(gpio_init) - 1; i >= 0; i--) {
        am335x_gpio_setup_pin_out(GPIO0, gpio_init[i].pin_nr, false);
        am335x_gpio_change_state(GPIO0, gpio_init[i].pin_nr, false);
    }
}

//turn off or on every one of the 7 segments
void display_segments(enum seg7_display digit, uint32_t value)
{
    uint32_t segments_to_show = 0;
    if ((value & SEG7_A) != 0) segments_to_show += SEGA;
    if ((value & SEG7_B) != 0) segments_to_show += SEGB;
    if ((value & SEG7_C) != 0) segments_to_show += SEGC;
    if ((value & SEG7_D) != 0) segments_to_show += SEGD;
    if ((value & SEG7_E) != 0) segments_to_show += SEGE;
    if ((value & SEG7_F) != 0) segments_to_show += SEGF;
    if ((value & SEG7_G) != 0) segments_to_show += SEGG;
    segments[digit].seg7 = segments_to_show;
}

//turn off or on the dots
void display_dots(bool show_dot)
{
    int dot = 0;
    if(show_dot) dot = DP1;
    segments[SEG7_LEFT].dot = dot;
}

//alternate between the right and left side to give the illusion to display elements on both sides at the same time
void refresh_display()
{
    //static holds the value between each function calls which permit to switch between segments
    static unsigned index = 0;

    // disable all segments
    am335x_gpio_change_states(GPIO0, SEG_ALL, false);
    am335x_gpio_change_state(GPIO2, DP1, false);

    if (index) {//right segment
        am335x_gpio_change_state(GPIO2, DIG1, false);
        am335x_gpio_change_state(GPIO2, DIG2, true);
        am335x_gpio_change_states(GPIO0, segments[index].seg7, true);

    } else {//left segment
        am335x_gpio_change_state(GPIO2, DIG2, false);
        am335x_gpio_change_state(GPIO2, DIG1, true);
        am335x_gpio_change_states(GPIO0, segments[index].seg7, true);
        am335x_gpio_change_state(GPIO2, segments[index].dot, true);
    }

    // circular table
    index = (index + 1) % 2;
}
