/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Abstract:    Introduction to C language and modular programming
 *
 * Purpose:     Make a serpentin navigating through the 7 segments display
 *
 * Author:      <Arton Hoxha & Valentin Antille>
 *
 * Date:        <01.10.2019>
 */

#include <bbb/am335x_gpio.h>
#include <stdint.h>
#include "serpentin.h"
#include "seg7.h"
#include "encoder.h"

//define the size of the array
#define ARRAY_SIZE(x) ((signed)(sizeof(x) / sizeof(x[0])))

//define the serpentin's path
static const struct serpentin {
	uint32_t left;
	uint32_t right;
} serpentin [] = {
	{SEG7_D, 0	    },
	{SEG7_C, 0	    },
	{0,      SEG7_G	},
	{0,      SEG7_C	},
	{0,      SEG7_D	},
	{0,      SEG7_E	},
	{0,      SEG7_F	},
	{SEG7_A, 0		},
	{SEG7_F, 0		},
	{SEG7_G, 0		},
	{SEG7_B, 0		},
    {0,      SEG7_A	},
	{0,      SEG7_B	},
	{0,      SEG7_G	},
	{SEG7_G, 0		},
	{SEG7_E, 0		},
};

static unsigned value = 0; //value that will be passed to move the serpentin

//initialize the serpentin
void serpentin_init()
{
	seg7_init();
	encoder_init();
}

//move the serpentin the long of its path
void serpentin_move (unsigned value)
{
	value = value % ARRAY_SIZE(serpentin);
    display_dots(false);	//deactivate the dots to avoid the left on staying alight when we switch from counter to serpentin
	display_segments (SEG7_LEFT,  serpentin[value].left);
	display_segments (SEG7_RIGHT, serpentin[value].right);
}

//get the wheel commands and update value accordingly
void serpentin_operations(){

    enum encoder_direction direction = encoder_direction();
        if (direction == LEFT) {
			value--;

		} else if (direction == RIGHT) {
			value++;
		}
	serpentin_move(value);
}


//reset the serpentin
void serpentin_reset()
{
	value = 0;
	serpentin_init();
}


