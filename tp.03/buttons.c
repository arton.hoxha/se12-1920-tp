/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Abstract:    Introduction to C language and modular programming
 *
 * Purpose:     Returns the state of the buttons if they are pressed or not
 *
 * Author:      <Valentin Antille>
 *
 * Date:        <01.10.2019>
 */

#include <bbb/am335x_gpio.h>
#include <stdbool.h>
#include <stdint.h>
#include "buttons.h"

//pin constant declarations
#define GPIO_1 AM335X_GPIO1
#define BTN1 15
#define BTN2 16
#define BTN3 17

//array which corresponds BTN enums to pins
static const uint32_t lut[] = {
    [BTN_1] = BTN1,
    [BTN_2] = BTN2,
    [BTN_3] = BTN3
};

//initialize buttons
void buttons_init(){
    am335x_gpio_init(GPIO_1);
    am335x_gpio_setup_pin_in(GPIO_1, BTN1, AM335X_GPIO_PULL_NONE, false);
    am335x_gpio_setup_pin_in(GPIO_1, BTN2, AM335X_GPIO_PULL_NONE, false);
    am335x_gpio_setup_pin_in(GPIO_1, BTN3, AM335X_GPIO_PULL_NONE, false);
}

//returns the state of the button if it is pressed or not
//am335x_gpio_get_state returns true if the button is not pressed.
bool buttons_get_state(enum btns_id btn){
    return !am335x_gpio_get_state(GPIO_1, lut[btn]);
}

