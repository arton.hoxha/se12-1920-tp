/**
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 3 Laboratory
 *
 * Abstract:    Peripheral devices implementation with C
 *
 * Purpose:     Get current, min and max values from thermo
 *
 * Author:      <Arton Hoxha>
 *
 * Date:        05.11.2019
 */

#include "thermo.h"

//constants declarations
#define I2C2 AM335X_I2C2
#define BUS_SPEED 400000
#define CHIP_ID 0x48
#define READ_REG 0

//two global variables to hold max and min values
static uint8_t min;
static uint8_t max;

//initialize the thermometer
void thermo_init()
{
    am335x_i2c_init(I2C2, BUS_SPEED);
    min = -20;
    max = 40;
}

//get the current value of the thermometer
//and change the min and max if needed
uint8_t thermo_read() {
    uint8_t data[2];
    am335x_i2c_read(I2C2, CHIP_ID, READ_REG, data, 2);
    uint8_t degree = data[0];
    if(degree > max){
        max = data[0];
    }else if(degree < min){
        min = data[0];
    }
    return data[0];
}

//get min value
uint8_t thermo_get_min(){
    return min;
}

//get max value
uint8_t thermo_get_max(){
    return max;
}
