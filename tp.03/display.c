/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 3 Laboratory
 *
 * Abstract:    Peripheral devices implementation with C
 *
 * Purpose:     Displays the thermometer and different values in the oled display
 *
 * Author:      <Arton Hoxha>
 *
 * Date:        <05.11.2019>
 */

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "display.h"
#include "bbb/font_8x8.h"
#include "oled.h"
#include "thermo.h"

// constant declaration
#define SCREEN_WIDTH 96
#define SCREEN_HEIGHT 96

#define LINE_WIDTH 3
#define LINE_SIZE 60

#define PX_VAL_0DEG 50 // pixel value of 0 degree

#define BLUE 0x0010
#define BLUE_SKY 0x041F
#define RED 0xf0a4
#define GREEN 0x07E0
#define BLACK 0
#define WHITE 0xFFFF

#define FONT_SIZE 7
#define RAYON_12 12
#define RAYON_8 8

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

//static points of different parts of the thermometer
static display_point line_left;
static display_point line_right;
static display_point circle_bottom;
static display_point circle_top;
static display_point rectangle;
static display_point line_graduation;
static display_point graduation_value;
static display_point degree_char;
static display_point rectangle_bottom;
static display_point rectangle_up;

//initialize all points coordinates
static void initialize_points(){
    line_left.x = 10;
    line_left.y = 44;
    line_right.x = 10;
    line_right.y = 52;

    circle_bottom.x = 80;
    circle_bottom.y = 48;
    circle_top.x = 10;
    circle_top.y = 48;

    rectangle.x = 6;
    rectangle.y = 44;

    line_graduation.x = 90;
    line_graduation.y = 35;

    graduation_value.y = 10;

    rectangle_bottom.y = 44;
    rectangle_up.x = 6;
    rectangle_up.y = 44;
}

//graduations and max min current values
char* graduations[] = {"-20", "0", "20", "40"};
char max[]          = {'0', '0', '0'};
char current[]      = {'0', '0', '0'};
char min[]          = {'0', '0', '0'};


//initialize the display
void display_init()
{
    oled_init(OLED_V101);
    initialize_points();
}


//displays a line
void display_line(display_point position, int size, uint32_t color, bool isHorizontal)
{
    if (isHorizontal) {
        oled_memory_size(position.x, position.x, position.y, position.y + size);
    } else {
        oled_memory_size(position.x, position.x + size, position.y, position.y);
    }

    for (int i = 0; i < size; i++) {
        oled_color(color);
    }
}

//displays a rectangle
void display_rectangle(display_point position, int width, int length, uint32_t color)
{
    oled_memory_size(position.x, position.x + length, position.y, position.y + width);
    for (int i = 0; i < (width + 1) * (length + 1); i++) {
        oled_color(color);
    }
}

//displays a circle
void display_circle(display_point position, int rayon, uint32_t color)
{
    int size = 0;
    for (int i = -(rayon); i <= rayon; i++) {
        size = sqrt((rayon * rayon) - (i * i));
        oled_memory_size(position.x - size, position.x + size, position.y + i, position.y + i);
        for (int r = 0; r <= (2 * size); r++) {
            oled_color(color);
        }
    }
}

//displays a char
void display_char(char c, display_point position, uint32_t color)
{
    oled_memory_size(position.x, position.x + FONT_SIZE, position.y, position.y + FONT_SIZE);

    for (int i = FONT_SIZE; i >= 0; i--) {
        for (int j = 0; j < FONT_SIZE + 1; j++) {
            if (fontdata_8x8[(int)c][j] & (1 << i)) {
                oled_color(color);
            } else {
                oled_color(BLACK);
            }
        }
    }
}

//displays a string
void display_string(char* word, display_point position, uint32_t color)
{
    // i just need the (x;y) coord and i can display a word
    display_point next_char = position;

    for (int i = 0; word[i] != '\0'; i++) {
        if (i == 0) {
            display_char(word[i], position, color);
        } else {
            next_char.y += FONT_SIZE;
            display_char(word[i], next_char, color);
        }
    }
}

//display basic thermo
void display_thermo()
{
    int width = 3;

    for (int i = 0; i < width; i++) {
        line_left.y -= 1;
        display_line(line_left, LINE_SIZE, WHITE, false);
        line_right.y += 1;
        display_line(line_right, LINE_SIZE, WHITE, false);
    }

    display_circle(circle_bottom, RAYON_12, WHITE);
    display_circle(circle_bottom, RAYON_8, BLUE_SKY);

    display_circle(circle_top, RAYON_8, WHITE);


    display_rectangle(rectangle, 8, 67, BLACK);

    for (unsigned int i = 0; i < ARRAY_SIZE(graduations); i++) {
        line_graduation.x -= 20;
        graduation_value.x = line_graduation.x - 5;
        display_line(line_graduation, 5, WHITE, true);
        display_string(graduations[i], graduation_value, WHITE);
    }
}

//display degrees on the right of the thermo
void display_degrees()
{
    degree_char.x = 15;
    degree_char.y = 65;
    sprintf(max, "%d", thermo_get_max());
    display_char('M', degree_char, RED);
    degree_char.y = 73;
    display_char('=', degree_char, RED);
    degree_char.y = 81;
    display_string(max, degree_char, RED);

    degree_char.x = 30;
    degree_char.y = 65;
    sprintf(current, "%d", thermo_read());
    display_char('c', degree_char, GREEN);
    degree_char.y = 73;
    display_char('=', degree_char, GREEN);
    degree_char.y = 81;
    display_string(current, degree_char, GREEN);

    degree_char.x = 45;
    degree_char.y = 65;
    sprintf(min, "%d", thermo_get_min());
    display_char('m', degree_char, BLUE);
    degree_char.y = 73;
    display_char('=', degree_char, BLUE);
    degree_char.y = 81;
    display_string(min, degree_char, BLUE);
}

//update thermo with new values
void display_thermo_update()
{
    int value = PX_VAL_0DEG - thermo_read();
    rectangle_bottom.x = value;
    display_rectangle(rectangle_bottom, 8, 73 - value, BLUE_SKY);
    display_rectangle(rectangle_up, 8, value - 7, BLACK);
}

//reinitialize the display
void display_clear()
{
    oled_memory_size(0, SCREEN_WIDTH - 1, 0, SCREEN_HEIGHT - 1);
    for (int i = 0; i < SCREEN_WIDTH * SCREEN_HEIGHT; i++) {
        oled_color(BLACK);
    }
}
