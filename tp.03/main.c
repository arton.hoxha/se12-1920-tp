/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 3 Laboratory
 *
 * Abstract:    Peripheral devices implementation with C
 *
 * Purpose:     Displays a thermometer and current, min max values
 *
 * Author:      <Arton Hoxha>
 *
 * Date:        <05.11.2019>
 */

#include "display.h"
#include "thermo.h"

int main()
{
    display_init();
    thermo_init();
    display_thermo();
    while (true) {
        display_degrees();
        display_thermo_update();
    }

    return 1;
}
