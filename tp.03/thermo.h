#pragma once
#ifndef THERMO_H
#define THERMO_H
/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 3 Laboratory
 *
 * Abstract:    Peripheral devices implementation with C
 *
 * Purpose:     Read temperature from thermometer and get min and max temp
 *
 * Author:      <Arton Hoxha>
 *
 * Date:        <05.11.2019>
 */

#include "bbb/am335x_i2c.h"
#include <stdint.h>


/**
 * Initialize the thermo
*/
void thermo_init();

/**
 * Get the current value of the thermo
 * and change max and min values if needed
 * @return current temperature
*/
uint8_t thermo_read();

/**
 * Get max value of the thermo
 * @return max temperature
*/
uint8_t thermo_get_max();

/**
 * Get min value of the thermo
 * @return min temperature
*/
uint8_t thermo_get_min();


#endif
