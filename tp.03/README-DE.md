# TP.03 : Implementierung von Peripheriegeräten mit C

## Ziele

Am Ende des Labors sind die Studenten/Studentinnen fähig,

* Modulare Programme (mehrere Dateien) in C zu konzipieren und zu realisieren
* Ein C Programm zu debuggen
* Den Gebrauch einer seriellen Schnittstelle (UART) zu beschreiben
  (vorbereitendes Studium der Signale und des Protokolls)
* Die Prinzipien eines seriellen Busses (I2C) zu beschreiben
  (vorbereitendes Studium der Signale und des Protokolls)
* Die Prinzipien eines seriellen Busses (SPI) zu beschreiben
  (vorbereitendes Studium der Signale und des Protokolls)
* das Konfigurationsmenü über den LCD-Bildschirm und den Drehencoder gestalten.
* Implementierung eines Thermometers mit einem I2C-Bus
* Implementierung eines LCD-Displays mit einem SPI-Bus
* Das datasheet eines einfachen elektronischen Bauteils zu verstehen

Dauer der praktischen Arbeit

* 2 Laboreinheiten (8 Stunden) + persönliche Arbeit

Abzugeben:

* Laborbericht und den Sourcecode auf das zentralisierte Depot

## Aufgaben

In diesem TP soll eine modulare Applikation in C konzipiert und realisiert werden,
welche es erlaubt, Peripheriegeräte mit I2C- und SPI-Bus auf der Erweitungskarte
zu implementieren. Ausserdem soll es eine Konfigurationsmenü über die OLED-Anzeige,
den Drehencoder und den Druckknopf realisiert werden.

Die zwei ausgewählten Peripheriegeräte sind:

- das LCD-Display OLED-C, welcher mit dem SPI-Bus verbunden ist.
  dieses wird auf den slot 1 (CAPE1) der Erweitungskarte gesteckt.
- das Thermo3 Thermometer, welches mit dem I2C-Bus verbunden ist.
  dieses wird auf den slot 2 (CAPE2) der Erweitungskarte gesteckt.

Das Programm besteht daraus, einen Digitalthermometer zu implementieren,
welcher die aktuell gemessene Temperatur sowie die Minimal- und Maximalwerte anzeigt.
(siehe folgende Abbildung)

Das Konfigurationsmenü ermöglicht es, mit der Anwendung zu interagieren.

![application](img/bbb.jpg)

Die Spezifikationen des Programmes:

* Thermometer
  * Die gemessene Temperatur des I2C-Thermometers wird konvertiert und in
	Grad Celcius angezeigt.
  * Die Minimal- und Maximalwerte werden in Echtzeit berechnet und in
	Grad Celcius angezeigt.

* LCD-Display
  * Das LCD-Display wird in zwei Teile unterteilt:
    * auf der linken Seite wird die aktuelle Temperatur, sowie die
	  Minimal- und Maximalwerte im Stil eines analogen Thermometers
	  von -20 bis +40°C angezeigt.
	* auf der rechten Seite werden die Temperaturen in Textform angezeigt (ascii).

  * Diese 3 Temperaturen werden in verschiedenen Farben angezeigt.

  * Die Orientierung der Anzeige soll so gewählt sein, dass wenn man das LCD-Display
    anschaut, es auch möglich sein soll den Text auf dem PCB der Karte zu lesen.
	d.h. Taster, 7-Segmentanzeige und Drehencoder befinden sich oberhalb des click boards.

* Konfigurationsmenü

  " Der LCD-Bildschirm und der Drehencoder mit seinem Druckknopf
    werden zur Implementierung eines Konfigurationsmenüs
    verwendet (siehe Abbildung unten).

    ![configuration](img/bbb2.jpg)

  * Es müssen mindestens 2 Menüs implementiert sein:
    * Ein Menü, in dem Sie die Farbe des Thermometers und die Farben der
      verschiedenen anzuzeigenden Temperaturen auswählen können.
      Es sollte eine Option zur Auswahl der Standardfarben vorgesehen werden.
    * Ein Menü, das es ermöglicht, mindestens die Minimal- und Maximaltemperatur
      auf den aktuellen Temperaturwert zu initialisieren.

## Nützliche Hinweise

Hier finden Sie einige Hinweise, welche die Umsetzung dieser praktischen
Arbeit erleichtert.

### I2C-Thermometer

Der Prozessor besitzt 3 I2C-Kontroller. Der 2. Kontroller (Bus I2C2) dient
als Schnittstelle zwischen dem µP und den Peripheriegeräten _microBUS_,
welche sich auf den slots "CAPE1" und "CAPE2" der Erweitungskarte befinden.
Das Modul [am335x_i2c.h](https://gitlab.forge.hefr.ch/embsys/libbbb/blob/master/src/bbb/am335x_i2c.h) der Bibliothek _`bbb`_ bietet die nötigen Dienste
zur Steuerung der Peripheriegeräte an.

Das click board "Thermo3" (I2C Gerät) ermöglicht es, die Umgebungstemperatur
zu erfassen. Die technischen Details zu diesem Gerät sind in den Kursunterlagen
oder direkt auf Gitlab ([se12/docs/01_datasheets/03_i2c/04_tmp102](https://gitlab.forge.hefr.ch/se12-1819/se12/blob/master/docs/01_datasheets/03_i2c/04_tmp102.pdf)) zu finden.

### LCD-Display OLED

Der Prozessor besitzt auch 2 SPI-Kontroller. Der SPI-Bus N°1 ist auch auf
den slots "CAPE1" und "CAPE2" der Erweitungskarte vorhanden.
Das Modul [am335x_spi.h](https://gitlab.forge.hefr.ch/embsys/libbbb/blob/master/src/bbb/am335x_spi.h) der Bibliothek _`bbb`_ bietet die nötigen Dienste
zur Steuerung der Peripheriegeräte an.

Das click board "OLED-C" (SPI-Gerät) bietet einen LCD-Display von
96x96 Pixel in RGB565 an. Die technischen Details zu diesem Display sind
in den Kursunterlagen oder direkt auf Gitlab ([se12/docs/01_datasheets/04_oled](https://gitlab.forge.hefr.ch/se12-1819/se12/tree/master/docs/01_datasheets/04_oled)).
Das Modul _`oled`_ bietet die Dienste zur Steuerung dieser Karte an.
Jedoch muss diese zwingend auf "CAPE1" verbunden sein.

Die Schriftart [font_8x8.h](https://gitlab.forge.hefr.ch/embsys/libbbb/blob/master/src/bbb/font_8x8.h) bietet eine "bitmap" für jeden der 256 Charakter
des ascii-Codes an. Diese kann natürlich auf die Bedürfnisse des Projektes
angepasst werden.

## Fragen

* Was sind die Haupteigenschaften (Signale und Protokoll) der
  Kommunikationsschnittstellen UART, I2C und SPI ?

* Wie kann man ein Bild drehen bevor es auf dem LCD-Display
  angezeigt wird ?

## Bedingungen

* Abgabe
    * Der Code und der Bericht werden auf das zentralisierte Git-Depot geladen
        * sources: _.../tp/tp.03_
        * Bericht: _.../tp/tp.03/doc/report.pdf_

* Frist
  * Der Code und der Bericht müssen bis spätestens 20 Tage nach dem TP
    bis 23h59 abgegeben werden.%

