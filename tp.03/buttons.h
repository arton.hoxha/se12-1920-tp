#pragma once
#ifndef BUTTONS_H
#define BUTTONS_H
/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Abstract:    Introduction to C language and modular programming
 *
 * Purpose:     Returns the state of the buttons if they are pressed or not
 *
 * Author:      <Valentin Antille>
 *
 * Date:        <01.10.2019>
 */

#include <stdbool.h>

//enum buttons for better readability
enum btns_id {BTN_1, BTN_2, BTN_3};

/**
 * initialization of the buttons
 */
void buttons_init();

/**
 * get the state of a button if it is pressed or not
 */
bool buttons_get_state(enum btns_id led);

#endif
