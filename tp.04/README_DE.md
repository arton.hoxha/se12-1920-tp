# TP.04 :  Implementierung eines Hardware-Timers in C

## Ziele

Am Ende des Labors sind die Studenten/Studentinnen fähig,

* Modulare Programme (mehrere Dateien) in C zu konzipieren und zu realisieren
* Ein C Programm zu debuggen
* Einen Peripherie-Treiber in C zu konzipieren und realisieren
* Einen Treiber für den DMTimer des AM3358 µP zu konzipieren
* Implementierung eines PWM-Gerätes
* Komponenten von früheren Arbeiten zu integrieren
* Das Datasheet eines Komponenten des Mikroprozessors zu verstehen

Dauer der praktischen Arbeit

* 2 Laboreinheiten (8 Stunden) + persönliche Arbeit

Abzugeben:

* Laborbericht und den Sourcecode auf das zentralisierte Depot

## Aufgaben

Das Ziel dieses TP ist es, ein Metronom zu entwerfen und zu implementieren.


![metronome](img/metronome.png)

Das System besteht aus den folgenden Komponenten:

- Drucktasten
- LEDs
- ein Drehencoder
- ein OLED-C LCD-Display
- ein PWM-Summer (buzzer)
- ein Timer


### Spezifikationen der Anwendung:

* Funktionalität
  * Tempo von 30 bis 250 Schlägen pro Minute
  * Taktzeit einstellbar für zwei-, drei- oder vierzählige Takte mit Anzeige auf dem LCD-Bildschirm
  * Visuelle Anzeige der Taktzeiten auf dem LCD-Bildschirm
  * Akustische Anzeige der Taktzeiten
  * Simulation der Balancebewegung auf dem LCD-Bildschirm
  * Erste Taktzeit besonders gekennzeichnet (abweichend von den anderen Taktzeiten)

* Drucktasten S1 bis S3
  * Drucktasten zur Auswahl der Anzahl der Taktzeiten (zwei-, drei- oder vierzählige Takte)

* LEDs L1 bis L3
  * Der Druck einer Taste wird auf der entsprechenden LED angezeigt.

* Drehencoder
  * Drucktaste zum Starten und Stoppen des Metronoms
  * Drehknopf zur Auswahl des Tempos (30 bis 250)

* Summer
  * Zeitanzeige (z. B. 1. Taktzeit bei 5'000Hz, andere Taktzeiten bei 3'000Hz)


### Taktgeber

  * Der Timer DMTimer2 des AM3358 µP dient als Taktgeber für die Applikation

  * Der Peripherie-Treiber steuert alle 6 Timer des µP,
    DMTimer 2 bis 7 und bietet die folgenden Funktionen an:
    - Eine Methode für die Initialisierung eines Timers
    - Eine Methode, um den aktuellen Wert eines Timers zu lesen
    - Eine Methode, um die Taktfrequenz eines Timers zu lesen
  * Das korrekte Funktionieren des Treibers muss bestätigt werden

## Nützliche Hinweise

Hier finden Sie einige Hinweise, welche die Umsetzung dieser praktischen
Arbeit erleichtert.

### Die Timer des AM3358 µP

Der µP verfügt über 8 verschiedene Timer (DMTimer 0 bis 7). Die Timer
2 bis 7 sind identisch. Die folgende Abbildung "Integration" zeigt deren
Integration im TI AM3358 µP.

Wie auf der Abbildung "Integration" gezeigt, erhalten die Timer ihre Clock vom
Modul _PRCM_. Dieses Modul bietet 3 verschiedene Clocks an, _CLK_M_OSC_ zu 24MHz,
_CLK_32KHz_ und eine externe Clock.
Das Modul [`am335x_clock.h`](https://gitlab.forge.hefr.ch/embsys/libbbb/blob/master/src/bbb/am335x_clock.h) stellt die notwendigen Dienste zum Benutzen der
verschiedenen Clocks des µP-Controllers zur Verfügung. Die Methode
_`am335x_clock_enable_timer_module(enum am335x_clock_timer_modules module)`_
erlaubt es, die Clock _CLK_M_OSC_ auszuwählen und den Takt des ausgewählten
Timers anzugeben.

![integration](img/integration.png)

([Ref: se12/docs/01_datasheets/01_am335x/06_am335x_technical_reference_manual.pdf, page 4328](https://gitlab.forge.hefr.ch/se12-1819/se12/blob/master/docs/01_datasheets/01_am335x/06_am335x_technical_reference_manual.pdf))

### Das Schema des DMTimer 2 bis 7

Die folgende Abbildung "dmtimer" zeigt das Schema des DMTimers 2 bis 7 des
AM3358 µP. Lediglich die Funktion _counter_ mit auto-reload und den Blöcken
_Prescaler_ und _Timer Counter_, sowie die Register _tclr_, _ttgr_, _tldr_, _tcrr_
und _tiocp_cfg_ werden bei der Realisierung dieses Projektes nötig sein. Die Logik
für die Unterbrechung (Interruption) ist für das Projekt nicht nützlich.

![dmtimer](img/dmtimer.png)

([Ref: se12/docs/01_datasheets/01_am335x/06_am335x_technical_reference_manual.pdf, page 4326](https://gitlab.forge.hefr.ch/se12-1920/lecture/blob/master/docs/01_datasheets/01_am335x/06_am335x_technical_reference_manual.pdf))

### Der Zählermodus des DMTimers

Die folgende Abbildung "Zählermodus" zeigt den Modus _Zähler_ der DMTimer.
In diesem Modus inkrementiert der Timer den Inhalt des Registers _tccr_ zu der
Taktfrequenz der Clock (in unserem Fall 24MHz), bis der Overflow-Wert 0xffff'ffff
erreicht ist.

Wenn man das Register _tclr_ mit dem Bit _AR_ auf 1 konfiguriert, wird der
Controller das Register _tccr_ mit dem Wert des Registers _tldr_ überschreiben.
Dieser Betriebsmodus ermöglicht es, einen Timer zu erhalten, welcher während einer
Periode von ca. 3 Minuten unendlich lange zählt.

![mode compteur](img/counter_mode.png)


([ref: se12/docs/01_datasheets/01_am335x/06_am335x_technical_reference_manual.pdf, chapitre 20.1.3.1, page 4331](https://gitlab.forge.hefr.ch/se12-1920/lecture/blob/master/docs/01_datasheets/01_am335x/06_am335x_technical_reference_manual.pdf))

### Die Register der DMTimer 2 bis 7

Die folgende Abbildung "Registers" zeigt die Register der DMTimer 2 bis 7.

Diese Register befinden sich im adressierbaren Bereich des µP an den folgenden Adressen (siehe Seiten 181-182)

* DMTimer2 : `0x4804_0000 - 0x4804_0FFF`
* DMTimer3 : `0x4804_2000 - 0x4804_2FFF`
* DMTimer4 : `0x4804_4000 - 0x4804_4FFF`
* DMTimer5 : `0x4804_6000 - 0x4804_6FFF`
* DMTimer6 : `0x4804_6000 - 0x4804_6FFF`
* DMTimer7 : `0x4804_A000 - 0x4804_AFFF`

![registers](img/regs.png)

([Ref: se12/docs/01_datasheets/01_am335x/06_am335x_technical_reference_manual.pdf, page 4340](https://gitlab.forge.hefr.ch/se12-1920/lecture/blob/master/docs/01_datasheets/01_am335x/06_am335x_technical_reference_manual.pdf))

### Buzzer

Der Prozessor verfügt über 3 PWM-Controller
Die erste und zweite Controller ermöglichen es der CPU, mit den MicroBus-Geräten in den CAPE1- und CAPE2-Steckplätzen der Erweiterungskarte zu kommunizieren.

Das Modul [am335x_epwm.h](https://gitlab.forge.hefr.ch/embsys/libbbb/blob/master/src/bbb/am335x_epwm.h) aus der libbb-Bibliothek stellt die notwendigen Dienste zur Verfügung, um solche Geräte zu betreiben.

Die technischen Details des Clickboards "Buzzer", eines PWM-Gerätes, sind
verfügbar in den Kursunterlagen oder direkt bei Gitlab
([se12/docs/01_datasheets/02_beaglebone/06_click_buzzer](https://gitlab.forge.hefr.ch/se12-1920/lecture/blob/master/docs/01_datasheets/02_beaglebone/06_click_buzzer.pdf)).

## Fragen

* Was ist die Bedeutung der Qualifiers _`volatile`_ und dessen Nutzen, wenn
  dieser an einen Pointer gebunden ist ?
* Wie werden die Felder (Elemente) einer Struktur im Speicher abgelegt ?
* Wie kann man die im Speicherbereich des µP abgelegten Register
  eines Peripherie-Controllers und deren Werte auf effiziente Weise definieren ?
* Wie kann man auf diese Register zugreifen ?
* Wie kann man Zufallszahlen generieren?
* Bei einer maximalen Frequenz von 24MHz, erlaubt es der Zähler des Timers nur in
  Intervallen von ca. 3 Minuten zu zählen. Beschreiben Sie den Algorithmus, welcher
  implementiert werden muss, wenn man während mehrerer Jahre mit der gleichen
  Genauigkeit zählen will.
* Was bedeutet die Abkürzung PWM?
* Wie funktioniert ein PWM?

## Aktualisierungen
* Der PWM-Treiber wurde in der Version 1.4.1 der Beaglebone Library (libbbb) eingeführt (_`libbbb`_).
  Hier sind die Schritte, die zu unternehmen sind, um es zu aktualisieren:

  * MacOS und Linux
    
    Öffnen Sie eine Shell und führen Sie Befehle aus:
    ```
    $ cd ~/workspace/baremetalenv
    $ git pull origin master
    $ ./install-libbbb.sh
    ```
  * Windows 10
    
    Öffnen Sie eine Shell _`WSL`_ und führen Sie Befehle aus:
    ```
    $ cd /usr/workspace/baremetalenv
    $ git pull origin master
    $ ./install-libbbb.sh
    ```

## Bedingungen

* Abgabe

  * Der Code und der Bericht werden auf das zentralisierte Git-Depot geladen
    * sources: _.../tp/tp.04_
    * rapport: _.../tp/tp.04/doc/report.pdf_

* Frist
  * Der Code und der Bericht müssen bis spätestens 20 Tage nach dem TP
    bis 23h59 abgegeben werden.

