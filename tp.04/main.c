/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 3 Laboratory
 *
 * Abstract:    Peripheral devices implementation with C
 *
 * Purpose:     Metronom with beats and bpm settings
 *
 * Author:      <Arton Hoxha, Samuel Fringeli and Léonard Noth>
 *
 * Date:        <24.12.2019>
 */

#include "display.h"
#include "timer.h"
#include "buzzer.h"
#include "encoder.h"
#include "metronom_display.h"
#include "buttons.h"
#include <stdbool.h>

uint32_t bpm = 60;
uint32_t time = 2;
bool metronom_sound_activated = false;

// set beats in metronom
void set_time(uint32_t time_to_set) {
    if (!metronom_sound_activated) {
        time = time_to_set;
        reset_time_counter();
        update_time(time);
    }
}

int main() {
    encoder_init();
    display_init();
    buzzer_init();
    timer_init();
    metronom_init();
    display_time_rects(time);
    display_bpm();
    update_bpm(bpm);
    update_time(time);

    while (true) {
        encoder_direction dir = encoder_get_direction();

        if (dir == RIGHT && bpm < 250 && !metronom_sound_activated) update_bpm(++bpm);
        if (dir == LEFT && bpm > 30 && !metronom_sound_activated) update_bpm(--bpm);

        if (buttons_get_state(BTN_1)) set_time(2);
        else if (buttons_get_state(BTN_2)) set_time(3);
        else if (buttons_get_state(BTN_3)) set_time(4);

        if (encoder_get_pressed() == PRESSED) {
            if (!metronom_sound_activated) {
                metronom_sound_activated = true;
                reset_time_counter();
                display_time_rects(time);
            } else {
                metronom_sound_activated = false;
                remove_current_pendulum();
                reset_pendulum();
                deactivate_sound();
            }
        }

        if (metronom_sound_activated) {
            uint64_t time_counter = get_time_counter(time);
            set_pendulum_goes_right(time_counter % 2 == 1);
            uint8_t prev_time = 0;
            if (time_counter != 0) prev_time = (time_counter - 1) % time;
            time_counter %= time;

            sound_state time_state = get_time_state(bpm, time);
            if (time_state == SOUND_HIGH || time_state == SOUND_LOW) {
                display_time_rect(prev_time, WHITE);
                display_time_rect(time_counter, BLUE_SKY);
                activate_sound(time_state == SOUND_HIGH);
            }
            else if (time_state == NO_SOUND_ACTIVATE) deactivate_sound();
            display_pendulum();
        }
    }

    return 1;
}
