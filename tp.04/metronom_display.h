#pragma once
#ifndef METRONOM_DISPLAY_H
#define METRONOM_DISPlAY_H

/**
* Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
* Project:     HEIA-FR / Embedded Systems 3 Laboratory
*
* Abstract:    Peripheral devices implementation with C
*
* Purpose:     Manages metronom display
*
* Author:      <Arton Hoxha, Samuel Fringeli and Léonard Noth>
*
* Date:        <24.12.2019>
*/

#include <stdint.h>
#include <stdbool.h>
#define PENDULUM_WIDTH 4

/**
 * Initializes metronom display (displays static variables)
*/
void metronom_init();

/**
 * Displays bpm static infos ("tempo" string)
*/
void display_bpm();

/**
 * Updates the bpm field
*/
void update_bpm(uint32_t bpm);

/**
 * Displays time static infos ("beats" string)
 */
void display_time();

/**
 * Updates the time field
*/
void update_time();

/**
 * Removes previous pendulum (little square) and re-display it at new coordinates
 * that depend on the timer informations, to create a smooth movement
*/
extern void display_pendulum();

/**
 * Setter for pendulum_goes_right
*/
extern void set_pendulum_goes_right(bool value);

/**
 * Displays time/beat square (rectangle)
 * @param rect_number : betwee 0 and 3, the number of the concerned time
 * @param color : color of the rectangle (can be black if we want to remove it)
*/
void display_time_rect(uint8_t rect_number, uint32_t color);

/**
 * Display all time squares
 * @param time : the number of squares to display
*/
extern void display_time_rects(uint32_t time);

/**
 * Set current pendulum pixels to black, used when metronom is paused
*/
extern void remove_current_pendulum();

/**
 * Set pendulum y coordinate to 0
*/
extern void reset_pendulum();

#endif
