/**
* Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
* Project:     HEIA-FR / Embedded Systems 3 Laboratory
*
* Abstract:    Peripheral devices implementation with C
*
* Purpose:     Handle events with rotary encoder
               (wheel left, right and button pressed)
*
* Author:      <Arton Hoxha, Samuel Fringeli and Léonard Noth>
*
* Date:        <24.12.2019>
*/

#include "encoder.h"
#include <bbb/am335x_gpio.h>

// GPIO of rotary encoder
#define CHA_GPIO AM335X_GPIO2
#define CHA_PIN 1
#define CHB_GPIO AM335X_GPIO1
#define CHB_PIN 29

// GPIO of rotary encoder button
#define CHC_GPIO AM335X_GPIO0
#define CHC_PIN 2

// left/right transitions
static const encoder_direction transition[4][4] = {
    {STILL, STILL, STILL, STILL},
    {LEFT, STILL, STILL, RIGHT},
    {RIGHT, STILL, STILL, LEFT},
    {STILL, STILL, STILL, STILL}
};

// button transitions
static const encoder_state pres_transition[2][2] = {
    {OFF, RELEASE},
    {PRESSED, ON}
};

static int former_state = 0;
static int button_state   = 0;

// get wheel direction state
static inline int get_direction_state() {
    int state = 0;
    if (am335x_gpio_get_state(CHA_GPIO, CHA_PIN)) state += 1;
    if (am335x_gpio_get_state(CHB_GPIO, CHB_PIN)) state += 2;
    return state;
}

// get wheel button state
static inline int get_pression_state() {
    int state = 0;
    if (am335x_gpio_get_state(CHC_GPIO, CHC_PIN)) state += 1;
    return state;
}

// initialize rotary encoder and GPIO
void encoder_init() {
    am335x_gpio_init(CHA_GPIO);
    am335x_gpio_init(CHB_GPIO);
    am335x_gpio_init(CHC_GPIO);

    am335x_gpio_setup_pin_in(CHA_GPIO, CHA_PIN, AM335X_GPIO_PULL_NONE, true);
    am335x_gpio_setup_pin_in(CHB_GPIO, CHB_PIN, AM335X_GPIO_PULL_NONE, true);
    am335x_gpio_setup_pin_in(CHC_GPIO, CHC_PIN, AM335X_GPIO_PULL_NONE, true);

    former_state = get_direction_state();
    button_state = get_pression_state();
}

// get rotary encoder direction
encoder_direction encoder_get_direction() {
    int state = get_direction_state();
    encoder_direction res = transition[former_state][state];
    former_state = state;
    return res;
}

// get rotary encoder button state
encoder_state encoder_get_pressed() {
    int state = get_pression_state();
    encoder_state res = pres_transition[button_state][state];
    button_state = state;
    return res;
}
