/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 4 Laboratory
 *
 * Abstract:    Peripheral devices implementation with C
 *
 * Purpose:     Manages buzzer
 *
 * Author:      <Arton Hoxha, Samuel Fringeli and Léonard Noth>
 *
 * Date:        <24.12.2019>
 */

#include "buzzer.h"
#include <stdbool.h>

// frequencies definition
#define TIME_BIP_SOUND 5000
#define BIP_SOUND 3000

#define EPWM1_CTR AM335X_EPWM1

void buzzer_init() {
    am335x_epwm_init(EPWM1_CTR);
    am335x_epwm_set_duty(EPWM1_CTR, 10);
}

void activate_sound(bool is_time_sound) {
    am335x_epwm_set_frequency(EPWM1_CTR, is_time_sound ? TIME_BIP_SOUND : BIP_SOUND);
}

void deactivate_sound() {
    am335x_epwm_set_frequency(EPWM1_CTR, 0);
}

void buzzer_set_frequency(uint32_t frequency) {
    am335x_epwm_set_frequency(EPWM1_CTR, frequency);
}
