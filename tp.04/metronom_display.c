/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 3 Laboratory
 *
 * Abstract:    Peripheral devices implementation with C
 *
 * Purpose:     Manages metronom display
 *
 * Author:      <Arton Hoxha, Samuel Fringeli and Léonard Noth>
 *
 * Date:        <24.12.2019>
 */

#include <stdio.h>
#include <stdlib.h>
#include "metronom_display.h"
#include "display.h"
#include "timer.h"

// width of time rectangle
#define TIME_WIDTH 10

// text to show
char* metronom_str[] = {"Tempo: ", "   ", "Beats: ", "   "};
bool pendulum_goes_right = true;

// coordinates of informations
display_point bpm_point = { .x = 70, .y = 10 };
display_point bpm_val_point = { .x = 70, .y = 60 };
display_point time_point = { .x = 80, .y = 10 };
display_point time_val_point = { .x = 80, .y = 60 };
display_point time_rect[] = {
    { .x = 10, .y = 10 },
    { .x = 10, .y = 30 },
    { .x = 10, .y = 50 },
    { .x = 10, .y = 70 }
};
display_point prev_pendulum_point = { .x = 40, .y = 0 };
display_point pendulum_point = { .x = 40, .y = 0 };

void metronom_init() {
    display_init();
    display_string(metronom_str[0], bpm_point, WHITE);
    display_string(metronom_str[2], time_point, WHITE);
}

void display_bpm() {
    display_string(metronom_str[1], bpm_val_point, WHITE);
}

void display_pendulum() {
    display_rectangle(prev_pendulum_point, PENDULUM_WIDTH, PENDULUM_WIDTH, BLACK);
    uint8_t pendulum_px = get_pendulum_px();
    if (!pendulum_goes_right) pendulum_px = SCREEN_WIDTH - pendulum_px;
    if (abs(pendulum_px - prev_pendulum_point.y) < PENDULUM_WIDTH) { // bug fix
        pendulum_point.y = pendulum_px;
    }
    prev_pendulum_point.y = pendulum_point.y;
    display_rectangle(pendulum_point, PENDULUM_WIDTH, PENDULUM_WIDTH, WHITE);
}

void update_bpm(uint32_t bpm) {
    sprintf(metronom_str[1], "%ld ", bpm);
    display_bpm();
}

void display_time_rect(uint8_t rect_number, uint32_t color) {
    display_rectangle(time_rect[rect_number], TIME_WIDTH, TIME_WIDTH, color);
}

void display_time_rects(uint32_t time) {
    for (uint8_t i = 0; i < 4; i++) {
        display_time_rect(i, i < time ? WHITE:BLACK);
    }
}

void display_time() {
    display_string(metronom_str[3], time_val_point, WHITE);
}

void update_time(uint32_t time) {
    sprintf(metronom_str[3], "%ld", time);
    display_time();
    display_time_rects(time);
}

void set_pendulum_goes_right(bool value) {
    pendulum_goes_right = value;
}

void remove_current_pendulum() {
    display_rectangle(pendulum_point, PENDULUM_WIDTH, PENDULUM_WIDTH, BLACK);
}

void reset_pendulum() {
    prev_pendulum_point.y = 0;
    pendulum_point.y = 0;
}
