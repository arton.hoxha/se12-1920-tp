/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 4 Laboratory
 *
 * Abstract:    Peripheral devices implementation with C
 *
 * Purpose:     Timer that does the job for the metronomelab
 *
 * Author:      <Arton Hoxha, Samuel Fringeli and Léonard Noth>
 *
 * Date:        <24.12.2019>
 */

#include "timer.h"
#include "display.h"
#include "metronom_display.h"
#include "buzzer.h"
#include <stdio.h>
#include <stdbool.h>

#define TIOCP_CFG_SOFTRESET (1 << 0)
#define TCLR_ST (1 << 0)
#define TCLR_AR (1 << 1)
#define BIP_DURATION 2500000
#define SCREEN_SIZE 96

uint64_t counter = 0;
sound_state prev_state = NO_SOUND;

uint32_t prev_bpm = 0;
uint32_t bpm_counter_max = 0;

struct dmtimer {
    uint32_t TIDR;
    uint32_t res[3];
    uint32_t TIOCP_CFG;
    uint32_t res1[3];
    uint32_t IRQ_EOI;
    uint32_t IRQSTATUS_RAW;
    uint32_t IRQSTATUS;
    uint32_t IRQENABLE_SET;
    uint32_t IRQENABLE_CLR;
    uint32_t IRQWAKEEN;
    uint32_t TCLR;
    uint32_t TCRR;
    uint32_t TLDR;
    uint32_t TTGR;
    uint32_t TWPS;
    uint32_t TMAR;
    uint32_t TCAR1;
    uint32_t TSICR;
};

static volatile struct dmtimer* timer_ctrl[] = {
    (volatile struct dmtimer*)0x48040000,
    (volatile struct dmtimer*)0x48042000,
    (volatile struct dmtimer*)0x48044000,
    (volatile struct dmtimer*)0x48046000,
    (volatile struct dmtimer*)0x48048000,
    (volatile struct dmtimer*)0x4804a000,
};

static const enum am335x_clock_timer_modules timer2clock[] = {
    AM335X_CLOCK_TIMER2,
    AM335X_CLOCK_TIMER3,
    AM335X_CLOCK_TIMER4,
    AM335X_CLOCK_TIMER5,
    AM335X_CLOCK_TIMER6,
    AM335X_CLOCK_TIMER7
};

void timer_init() {
    am335x_clock_enable_timer_module(timer2clock[0]);
    volatile struct dmtimer* ctrl = timer_ctrl[0];
    ctrl->TIOCP_CFG               = TIOCP_CFG_SOFTRESET;
    while ((ctrl->TIOCP_CFG & TIOCP_CFG_SOFTRESET) != 0);

    ctrl->TLDR = 0;
    ctrl->TCRR = 0;
    ctrl->TTGR = 0;
    ctrl->TCLR = TCLR_AR | TCLR_ST;
}

uint32_t bpm_to_counter(uint32_t bpm) {
    return 1440000000 / bpm;
}

void reset_time_counter() {
    counter = 0;
}

uint8_t get_pendulum_px() {
    volatile struct dmtimer* ctrl = timer_ctrl[0];
    return (ctrl->TCRR * (SCREEN_WIDTH - 1 - PENDULUM_WIDTH)) / bpm_counter_max;
}

sound_state get_time_state(uint32_t bpm, uint32_t time) {
    sound_state state = SAME;
    if (bpm != prev_bpm) {
        prev_bpm = bpm;
        bpm_counter_max = bpm_to_counter(bpm);
    }

    volatile struct dmtimer* ctrl = timer_ctrl[0];
    if (ctrl->TCRR > bpm_counter_max) {
        ctrl->TTGR |= 1; // resets counter
        state = (counter++ % time == 0) ? SOUND_HIGH : SOUND_LOW;
    }

    if (ctrl->TCRR > BIP_DURATION) {
        state = (prev_state == NO_SOUND || prev_state == NO_SOUND_ACTIVATE) ? NO_SOUND : NO_SOUND_ACTIVATE;
    }

    prev_state = state;
    return state;
}

uint64_t get_time_counter() {
    return counter;
}
