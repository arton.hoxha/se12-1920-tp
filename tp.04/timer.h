#pragma once
#ifndef TIMER_H
#define TIMER_H

/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 3 Laboratory
 *
 * Abstract:    Peripheral devices implementation with C
 *
 * Purpose:     Timer that does the job for the metronomelab
 *
 * Author:      <Arton Hoxha, Corentin Bompard>
 *
 * Date:        <25.03.2020>
 */

#include <bbb/am335x_clock.h>
#include <bbb/am335x_epwm.h>
#include <stdbool.h>


typedef enum {SOUND_HIGH, SOUND_LOW, NO_SOUND_ACTIVATE, NO_SOUND, SAME} sound_state;

/**
 * Converts bpm to the value above which the counter must be reseted
*/
uint32_t bpm_to_counter(uint32_t bpm);

/**
 * Initializes timer
*/
extern void timer_init();

/**
 * Gets the number of times already elapsed
*/
extern uint64_t get_time_counter();

/**
 * Gets the state (in px for the screen) of the current time counter
*/
extern uint8_t get_pendulum_px();

/**
 * Resets counter for number of times already elapsed
*/
extern void reset_time_counter();

/**
 * Gets the current sound state to allows main.c to correctly manage the buzzer
*/
extern sound_state get_time_state(uint32_t bpm, uint32_t time);

#endif
