#pragma once
#ifndef ENCODER_H
#define ENCODER_H

/**
* Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
* Project:     HEIA-FR / Embedded Systems 3 Laboratory
*
* Abstract:    Peripheral devices implementation with C
*
* Purpose:     Handle events with rotary encoder
               (wheel left, right and button pressed)
*
* Author:      <Arton Hoxha, Samuel Fringeli and Léonard Noth>
*
* Date:        <24.12.2019>
*/

#include <stdio.h>
#include <stdint.h>

typedef enum {STILL, RIGHT, LEFT} encoder_direction;
typedef enum {ON, OFF, PRESSED, RELEASE} encoder_state;

/**
 * Initialize rotary encoder
*/
extern void encoder_init();

/**
 * Get rotary encoder direction
*/
extern encoder_direction encoder_get_direction();


/**
 * Get if rotary encoder button is pressed
*/
extern encoder_state encoder_get_pressed();

#endif
