#pragma once
#ifndef BUZZER_H
#define BUZZER_H
/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 3 Laboratory
 *
 * Abstract:    Peripheral devices implementation with C
 *
 * Purpose:     Manages buzzer
 *
 * Author:      <Arton Hoxha, Samuel Fringeli and Léonard Noth>
 *
 * Date:        <24.12.2019>
 */

#include <bbb/am335x_clock.h>
#include <bbb/am335x_epwm.h>
#include <stdbool.h>

/**
 * initialization of the buzzer
*/
void buzzer_init();

/**
 * set frequency (in Hz) of the buzzer
*/
void buzzer_set_frequency();

/**
 * public method to activate the buzzer sound
 * @param bool is_time_sound sets a higher frequency for the sound of the first beat
*/
extern void activate_sound(bool is_time_sound);

/**
 * shuts down any buzzer sound
*/
extern void deactivate_sound();

#endif
