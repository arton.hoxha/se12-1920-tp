# Travaux pratiques de la classe I2a/I2d

<img src="img/se-i2a.png">

# Travaux pratiques de la classe I2b/T2a/T2f/T2d (printemps)

<img src="img/se-i2b_t2a_t2f_t2d.png">

# Répartition des groupes pour les classes I2a et I2d

| Groupe A             | Groupe B                  |
|----------------------|---------------------------|
|  1. Lucas Bueche     |  1. Romain Agostinelli    |
|  2. Aurélie Descloux |  2. Sébastien Blumenstein |
|  3. Joé Donzallaz    |  3. Corentin Bompard      |
|  4. Dylan Mamié      |  4. Loris Bruno           |
|  5. Timothée Monney  |  5. Mickaël Carvalho      |
|  6. Justin Nydegger  |  6. Samuel Fringeli       |
|  7. Julien Piguet    |  7. Jérémy Gamba          |
|  8. Denis Rosset     |  8. Arton Hoxha           |
|  9. Loris Roubaty    |  9. Jonathan Jacquat      |
| 10. Paul Roulin      | 10. Loris Kenklies        |
| 11. Julien Torrent   | 11. Léonard Nikita Noth   |
| 12. Jérôme Vial      | 12. Cédric Tâche          |

## Dates des TPs

| TP | Groupe A   | Groupe B   |
|----|------------|------------|
|  1 | 19 février | 26 février |
|  2 | 4 mars     | 11 mars    |
|  3 | 18 mars	  | 25 mars    |
|  4 | 1 avril	  | 8 avril    |
|  5 | 22 avril	  | 29 avril   |
|  6 | 5 mai	  | 13 mai     |
|  7 | 27 mai     | 3 juin     |
|  8 | 10 juin    | 17 juin    |

## Dates des travaux écrits

- 8 avril à 13h00
- 10 juin à 13h00
