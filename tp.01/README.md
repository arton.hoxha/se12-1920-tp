# Systèmes Embarqués 1 - TP.01 : Introduction

## Objectifs

A la fin du laboratoire, les étudiant-e-s seront capables de

* Décrire les processus de développement en micro-informatique
* Appliquer les règles de fonctionnement du travail en laboratoire
* Trouver le matériel et la documentation dans le laboratoire
* Décrire les processus d’assemblage et d’édition de liens d’une application 
  (compilée ou assemblée)
* Manipuler et connecter correctement les cibles
* Manipuler les fonctions de base de l’environnement de développement
* Décrire les caractéristiques principales du microprocesseur
* Analyser le déroulement d’un programme élémentaire

Durée

* 1 séance de laboratoire (4 heures)

Rapport

 * Journal de laboratoire

## Cible

Durant les travaux pratiques, nous allons utiliser la cible Beaglebone Black  
![Beaglebone Black - La cible][bbb_target]

voici ses caractéristiques principales (https://elinux.org/Beagleboard:BeagleBoneBlack)  
![Beaglebone Black - Les Features][bbb_features]

Le microprocesseur de TI AM3358 est un µP ARM Cortex-A8, lequel dispose des 
registres suivants:  
![Beaglebone Black - Les registres][bbb_registers]

[bbb_target]: img/fig1.png
[bbb_features]: img/fig2.png
[bbb_registers]: img/fig3.png

## Processus de développement logiciel

Le développement de logiciel suit les étapes suivantes

* Le code source assembleur, C ou autre est écrit dans des fichiers texte 
(ascii) à l’aide d’un éditeur de texte.

* Le code source est ensuite compilé pour obtenir le code binaire "relogeable"
exécutable par le µP. Une phase de correction liée à l'écriture correcte du
code source (syntaxe) est naturellement comprise dans cette partie du processus.

* Le fichier binaire est lié à l'architecture matérielle. Cette étape se divise
en deux phases. La première consiste à résoudre les liens entre les différents
symboles (p.ex. appel de fonction, adresses de variables globales...). Si
des symboles restent indéfinis, une phase de correction devra être entreprise.
La deuxième consiste à reloger le code selon l'organisation mémoire de la cible.

* Le code est "chargé" en mémoire sur le système cible (système de test ou 
simulateur). Il est ensuite testé de manière exhaustive. Une série de 
corrections liées au mauvais fonctionnement du programme (bugs) peuvent 
intervenir à cette étape.  
![Toolchain][toolchain]

[toolchain]: img/fig4.png

> **Attention:**
> Des erreurs dues à une mauvaise conception (design) de l'application qui
> apparaissent lors de sa vérification peuvent être très complexes à 
> identifier et coûter beaucoup d'énergie et d'argent pour être corrigées!


## Développement de logiciel pour systèmes embarqués

Le développement de logiciel pour des systèmes embarqués s'effectue 
généralement de manière croisée. Pour cela, nous allons utiliser une 
machine hôte pour l'édition du code et la génération de l'exécutable 
pour la cible. Quant à son débugging, nous allons utiliser la machine 
hôte pour le débugger et la cible pour exécuter l'application. 
Le débugger interagit avec la cible via une interface spécialisée. 
Dans notre cas, la sonde JLink de la maison Segger servira d'interface 
entre la machine hôte et la cible. Elle offre une interface UBS/JTAG.
![Development][development]

**Sur l'hôte**

* Développement de l’application
    * Edition code source
    * Compilation / Assemblage
    * Edition de liens (linking)
* Débogueur
    * Code source
    * Symboles / adresses
* Terminal (optionnel)
    * Commandes
    * Tracing / logging  

**Sur la cible**

* Exécution de l’application
    * Code binaire
* Serveur de débogage
    * Agent (i/f avec débogueur)  
![Interfaces][interfaces]

[development]: img/fig6.png
[interfaces]: img/fig7.png


## Outils de développement

Pour le développement,nous n’utiliserons principalement des outils libres 
sous Windows 10, macOS 10.14.6, Fedora 30 ou Ubuntu 19.04

* Visual Studio Code 1.37.1, avec les extensions C/C++ (ms-vscode.cpptools) 
  et ARM (dan-c-underwood.arm) 
* GNU-toolchain, outils de développement d’applications, avec
    * Les binutils (assembler, linker, ...) version 2.32
    * Le compilateur C version 8.3.1
    * Le débogueur GDB version 8.3.0
    * Le make version 4.2.1
    * La librairie standard newlib 3.0 de RedHat
* Git pour la gestion de tout le code source
* J-Link Software V6.48b, utilitaire permettant d’interfacer la chaîne 
  d’outil GNU avec la cible au travers d’une interface USB/JTAG
* Une configuration de projet spécifique HEIA-FR pour notre cible  
![Outils][outils]

[outils]: img/fig8.png


## Organisation de l’espace de travail et des dépôts Git

Le GitLab de l'école servira à la distribution du support de cours ainsi que
des données des travaux pratiques. Pour les étudiants, il devra être utilisé
pour le rendu des TP. 

La figure suivante représente schématique l'infrastructure. Le dépôt "upstream"
contient les documents fournis par le professeur, tandis que le dépôt "origin" 
servira pour le rendu des étudiants. Un des 2 étudiants devra créer le dépôt 
"origin", tandis que le 2e devra le cloner.  
![Gitlab][gitlab]

Sur les machines de travail des étudiants, il est recommandé de créer un 
répertoire "workspace" dans lequel on créera un sous-répertoire "se12". 
Dans ce répertoire, il faudra cloner le dépôt "lecture" ainsi que le 
dépôt "tp".   
![Workspace][workspace]

[gitlab]: img/fig9.png
[workspace]: img/fig10.png

## Installation de l’environnement de développement

Chaque étudiant peut choisir son environnement de développement entre 3 options

* Option 1 : Travailler avec sa machine personnelle macOS.
* Option 2 : Travailler avec sa machine personnelle Linux. 
             Ubuntu et Fedora sont supportés.
* Option 3 : Travailler avec sa machine personnelle Windows 10 
             sous l'environnement WSL.  

> **Remarque:** 
> avant de lancer les scripts d'installation ci-dessous, il est recommandé de 
> vérifier que leur contenu soit compatible avec l'installation de la machine 
> hôte. 

### Machine Mac OS X

Pour installer l'environnement de développement sous une machine macOS, 
il suffit de suivre les instructions ci-dessous:

* Installer HomeBrew, si pas encore fait (https://brew.sh/)

* Installer Git
```
    $ brew install git
```

* Configurer Git
```
    $ git config --global user.name <Firstname Lastame>
    $ git config --global user.email <user.name>@edu.hefr.ch
```

* Créer l’espace de travail (workspace)
```
    $ mkdir -p ~/workspace
```
  
* Cloner le dépôt Git contenant les scripts d'installation de l'environnement. 
  Si demandé, utiliser le _username_ et _password_ personnel de l'école.
```
    $ cd ~/workspace
    $ git clone https://gitlab.forge.hefr.ch/embsys/baremetalenv.git
```

* Installer l’environnement les outils de développement pour la cible
```
    $ ./baremetalenv/install-bbb-macos-env.sh
```

* Installer la bibliothèque libbbb
```
    $ ./baremetalenv/install-libbbb.sh
```


### Machine Linux

Pour installer l'environnement de développement sous une machine Linux 
(Ubuntu ou Fedora), il suffit de suivre les instructions ci-dessous:

* Installer Git
    * Ouvrir un terminal (une shell Linux) et créer le répertoire de travail 
      (workspace). Si demandé, utiliser le _username_ et _password_ personnel 
      de l'école.
      * Ubuntu: `$ sudo apt install -y git`
      * Fedora: `$ sudo dnf install -y git`

* Configurer Git
```
    $ git config --global user.name <Firstname Lastame>
    $ git config --global user.email <user.name>@edu.hefr.ch
```

* Créer l’espace de travail (workspace)
```
    $ mkdir -p ~/workspace
```

* Cloner le dépôt Git contenant les scripts d'installation de l'environnement.
  Si demandé, utiliser le _username_ et _password_ personnel de l'école .
```
    $ cd ~/workspace
    $ git clone https://gitlab.forge.hefr.ch/embsys/baremetalenv.git
```
    * Lorsque demandé, utiliser le _username_ et _password_ personnel de l'école 

* Installer l’environnement les outils de développement pour la cible
    * Ubuntu: ```$ ./baremetalenv/install-bbb-ubuntu-env.sh```
    * Fedora: ```$ ./baremetalenv/install-bbb-fedora-env.sh```

* Installer la bibliothèque libbbb
```
    $ ./baremetalenv/install-libbbb.sh
``` 


### Machine Windows 10

Pour installer l'environnement de développement sous une machine Windows 10, 
il suffit de suivre les instructions ci-dessous.

* Installer Windows Subsystem for Linux (_`WSL`_), si pas encore fait
    * Ouvrir une PowerShell comme adminstrateur et entrer la commande :   
    ```
        Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
    ```
    * Redemarrer la machine

* Installer Ubuntu 18.04 LTS, si pas encore fait:
    * Aller sur "Microsoft Store" et installer Ubuntu 18.04 LTS
        * Il n'est pas nécessaire de signer pour obtenir l'application
    * Entrer votre nom d'utilisateur et votre password personnel
    * Autoriser l'utilisateur à effectuer des opérations "sudo" sans password
        * Taper la commande: ```sudo visudo```
        * Modifier la ligne `%sudo ...` comme suit:
        ```
            %sudo   ALL=(ALL:ALL) NOPASSWD: ALL
        ```
    * Mettre à jour la distribution
    ```
        $ sudo apt update
        $ sudo apt upgrade -y
    ```
    * Redémarrer la machine Windows 10

* Installer Visual Code Studio
    * Télécharger Studio Code du site _`https://code.visualstudio.com/`_ 
    * Installer VS-Code sous Windows 10
    * Installer l'extension _`Remote - WSL`_ _(ms-vscode-remote.remote-wsl)_
    * Installer l'extension _`C/C++`_ _(ms-vscode.cpptools)_
    * Installer l'extension _`ARM`_ _(dan-c-underwood.arm)_
    * Installer l'extension _`EditorConfig for VS Code`_ _(editorconfig.editorconfig)_

* Installer Visual Code Studio Server sous _`WSL`_
    * Ouvrir une shell _`WSL`_ et entrer la commande: ```$ code```
    * Fermer VS-Code une fois installé et entrer la commande: ```$ code```
    * Installer l'extension _`C/C++`_ `(ms-vscode.cpptools)` dans _`WSL`_
    * Fermer VS-Code une fois installé

* Créer l’espace de travail (workspace) sous Windows 10
    * Créer sous Windows 10 un répertoire _`workspace`_

* Créer l’espace de travail (workspace) sous Windows 10 _`WSL`_
    * Ouvrir une shell _`WSL`_ et exécuter la commande: ```$ sudo mkdir -p /usr/workspace```
    * Ouvrir le fichier _`~/.bashrc`_ et ajouter la ligne suivante à fin du fichier:  
        _`sudo mount --bind "/mnt/c/Users/<Windows\ Home\ Directory>/workspace" /usr/workspace `_  
        > **Remarque:** mettre en guillemets le path `/mnt/c/Users/...`
    * Sauver le fichier et quitter la shell

* Installer Git pour Windows 10
    * Télécharger Git du site _`https://git-scm.com/`_ 
    * Installer Git sous Windows 10 (utiliser les valeurs par défaut)

* Configurer Git pour _`WSL`_
    * Ouvrir une shell _`WSL`_ et exécuter les commandes
    ```
        $ git config --global user.name <Firstname Lastame>
        $ git config --global user.email <user.name>@edu.hefr.ch
    ```

* Cloner le dépôt Git contenant les scripts d'installation de l'environnement.
  Si demandé, utiliser le _username_ et _password_ personnel de l'école.
```
    $ cd /usr/workspace
    $ git clone https://gitlab.forge.hefr.ch/embsys/baremetalenv.git
```

* Installer l’environnement les outils de développement pour la cible
```
    $ ./baremetalenv/install-bbb-w10wsl-env.sh
```

* Installer la bibliothèque libbbb
```
    $ ./baremetalenv/install-libbbb.sh
```

* Installer les outils JLink
    * Aller avec le "file explorer" de Windows sous le répertoire 
    _`workspace\baremetalenv\config`_  et lancer l'application _`JLink_Windows_V648b.exe`_
    * Suivre les instructions par défaut

## Création du dépôt centralisé et configuration de l'espace de travail

Une fois l'environnement de développement mis en place, il faut créer le dépôt
centralisé sur le GitLab de l'école et configurer l'espace de travail sur
la machine hôte.

* Créer le dépôt de groupe pour les tp (1 par groupe de 2 personnes)
    * Avec un browser aller sur le Git de l’école (https://gitlab.forge.hefr.ch/)
    * Sélectionner l’onglet `Projects` et cliquer `+ New Project`
    * Nommer le projet `se12-1920-tp` et cliquer `Create Project`
    * Noter l’URL du projet (https://gitlab.forge.hefr.ch/\<username\>/se12-1920-tp)
    * Ajouter le 2ème étudiant comme `Maintainer` au projet  
      (https://gitlab.forge.hefr.ch/\<username\>/se12-1920-tp/-/project_members)
    * Ajouter le professeur comme `Reporter` (au minimum) au projet  
      (https://gitlab.forge.hefr.ch/\<username\>/se12-1920-tp/-/project_members)

* Ouvrir un terminal (shell) - sous Windows un _`WSL`_ -

* Créer l'espace de travail sur la machine hôte
    * Ubuntu, Fedora et macOS
    ```
        $ mkdir -p ~/workspace/se12
        $ cd ~/workspace/se12
    ```  
    * Windows 10 / WSL
    ```
        $ mkdir -p /usr/workspace/se12
        $ cd /usr/workspace/se12
     ```

* Installer les dépôts du cours
```
    $ git clone https://gitlab.forge.hefr.ch/se12-1920/lecture.git
    $ git clone -o upstream https://gitlab.forge.hefr.ch/se12-1920/tp.git
```

* Ajouter le dépôt `tp` du cours au dépôt local et synchroniser le avec le dépôt de groupe
```
    $ cd tp
    $ git remote add -t master -m master origin \
      https://gitlab.forge.hefr.ch/<username>/se12-1920-tp.git
    $ git push -u origin master
```

## Test de l'environnement de développement

L'environnement de développement est maintenant en place. Il est possible de 
le tester avec une première application.

* Ouvrir un terminal (shell) - sous Windows un _`WSL`_ - et exécuter les 
  commandes ci-dessous:
    * Ubuntu, Fedora et macOS
    ```
        $ cd ~/workspace/se12/tp/tp.01
        $ make
        $ code .
    ```  
    * Windows 10 / WSL
    ```
        $ cd /usr/workspace/se12/tp/tp.01
        $ make
        $ code .
     ```

* Configurer les chemins des fichiers d'en-tête
    * Ouvrir la `Command Palette` avec la combinaison "`command shift p`"
    * Choisir `C/C++: Edit Configurations (JSON)`
    * Remplacer les lignes 
    ```json
        "includePath": [
                    "${workspaceFolder}/**"
                ],
    ```
    * par les lignes 
        * macOS:
        ```json
            "includePath": [
                        "${workspaceFolder}/**",
                        "/usr/local/Cellar/arm-gcc-bin/**"
                    ],
        ```
        * Ubuntu et Fedora:
        ```json
            "includePath": [
                        "${workspaceFolder}/**",
                        "/usr/local/arm-none-eabi/**"
                    ],
        ```
        * Windows 10 / WSL:
        ```json
            "includePath": [
                        "${workspaceFolder}/**",
                        "/usr/local/arm-none-eabi/**",
                        "/usr/local/lib/gcc/arm-none-eabi/**"
                    ],
            "compilerPath": "/usr/local/bin/arm-none-eabi-gcc",
        ```

* Tester la connexion entre la cible et la machine hôte
    * Connecter la sonde JLink entre la cible et la machine hôte
    * Connecter la ligne série entre la cible et la machine hôte
    * Alimenter la cible
    * Lancer le `GDB-Server`, taper dans le terminal (shell) la commande
        * macOS, Ubuntu et Fedora : ```$ JLinkGDBServer -device am3358```
        * Windows 10 / WSL
        ```
            $ /mnt/c/Program\ Files\ \(x86\)/SEGGER/JLink/JLinkGDBServerCL.exe -device am3358
        ```
    * Dans le terminal (shell), l'info suivante doit s'afficher:
    ```
        Connected to target
        Waiting for GDB connection...
    ```
    * Stoper le `GDB-Server` avec la combinaison "`Ctrl-c`"

* Ouvrir un terminal `minicom` avec la cible
    * Configuration de `minicom` avec la commande: ```$ minicom -s```
    * Sous `Serial port setup`
        * Serial Device: 
            * macOS: `/dev/tty.usbserial-...`
            * Ubuntu et Fedora: `/dev/ttyUSB0`
            * WSL: `/dev/ttySx` où `x` correspond au numéro du `COMx`
        * Bps/Par/Bits: `115200 8N1`
        * Hardware Flow Control: `No`
        * Software Flow Control: `No`
    * Sauvergarder comme default et sortir

* Débugger l'application
    * Ouvrir le débugger (icône ou combinaison "`command shift d`")
    * Configurer le débugger en cliquant sur l'icône. 
    * Choisir `C/C++ (GDB/LLDB)`
    * Remplacer le contenu du fichier avec les lignes suivantes
        * macOS:  
    ```json
        {
            // Use IntelliSense to learn about possible attributes.
            // Hover to view descriptions of existing attributes.
            // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
            "version": "0.2.0",
            "configurations": [
                {
                    "name": "(arm-gdb) Launch",
                    "type": "cppdbg",
                    "request": "launch",
                    "program": "${workspaceFolder}/app_a",
                    "stopAtEntry": true,
                    "cwd": "${workspaceFolder}",
                    "MIMode": "gdb",
                    "debugServerPath": "/usr/local/bin/JLinkGDBServer",
                    "debugServerArgs": "-device am3358",
                    "miDebuggerPath": "/usr/local/bin/arm-none-eabi-gdb",
                    "setupCommands": [
                        { "text": "set architecture armv7"       },
                        { "text": "file ${workspaceRoot}/app_a"  },
                        { "text": "target remote localhost:2331" },
                        { "text": "monitor reset"                },
                        { "text": "monitor go"                   },
                        { "text": "monitor sleep 100"            },
                        { "text": "monitor halt"                 },
                        { "text": "load"                         },
                    ]
                }
            ]
        }        
    ```
        * Ubuntu et Fedora:  
    ```json
        {
            // Use IntelliSense to learn about possible attributes.
            // Hover to view descriptions of existing attributes.
            // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
            "version": "0.2.0",
            "configurations": [
                {
                    "name": "(arm-gdb) Launch",
                    "type": "cppdbg",
                    "request": "launch",
                    "program": "${workspaceFolder}/app_a",
                    "stopAtEntry": true,
                    "cwd": "${workspaceFolder}",
                    "MIMode": "gdb",
                    "debugServerPath": "/usr/bin/JLinkGDBServer",
                    "debugServerArgs": "-device am3358",
                    "miDebuggerPath": "/usr/local/bin/arm-none-eabi-gdb",
                    "setupCommands": [
                        { "text": "set architecture armv7"       },
                        { "text": "file ${workspaceRoot}/app_a"  },
                        { "text": "target remote localhost:2331" },
                        { "text": "monitor reset"                },
                        { "text": "monitor go"                   },
                        { "text": "monitor sleep 100"            },
                        { "text": "monitor halt"                 },
                        { "text": "load"                         },
                    ]
                }
            ]
        }        
    ```
        * Windows 10:
    ```json
        {
            // Use IntelliSense to learn about possible attributes.
            // Hover to view descriptions of existing attributes.
            // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
            "version": "0.2.0",
            "configurations": [
                {
                    "name": "(arm-gdb) Launch",
                    "type": "cppdbg",
                    "request": "launch",
                    "program": "${workspaceFolder}/app_a",
                    "stopAtEntry": true,
                    "cwd": "${workspaceFolder}",
                    "MIMode": "gdb",
                    "debugServerPath": "/mnt/c/Program Files (x86)/SEGGER/JLink/JLinkGDBServerCL.exe",
                    "debugServerArgs": "-device am3358",
                    "miDebuggerPath": "/usr/local/bin/arm-none-eabi-gdb",
                    "setupCommands": [
                        { "text": "set architecture armv7"       },
                        { "text": "file ${workspaceRoot}/app_a"  },
                        { "text": "target remote localhost:2331" },
                        { "text": "monitor reset"                },
                        { "text": "monitor go"                   },
                        { "text": "monitor sleep 100"            },
                        { "text": "monitor halt"                 },
                        { "text": "load"                         },
                    ]
                }
            ]
        }        
    ```

    * Sauver le fichier 
    * Lancer le debugging et regarder votre cible ainsi que la sortie 
      sur le terminal


## GNU Debugger (GDB)

VS-Code offre une console de debugging. Via cette console il est possible 
d'exécuter des commandes du debugger de GNU (GDB). En voici un bref résumé 
rapide: [GDB Quick Reference](img/08_gnu-gdb-refcard.pdf)

Pour exécuter une commande dans la console de débugger, il faudra précéder
chaque commande de l'instruction `-exec`. Par exemple pour obtenir la liste
des registres du processeur et du contenu, il faut entrer la commande: 
`-exec info registers`.


## Mise en page

Les extensions VS-Code `C/C++` et `Clang-Format` permettent une mise en page 
uniforme de tous les fichiers sources. 

Le fichier `.editorconfig`, placé la racine du dépôt des TP et utilisé par
l'extension `EditorConfig for VS Code`, permet de configurer VS-Code sur
le format des différents fichiers d'un projet (code source, Makefile, ...).
Pour plus de détails, consulter la documentation de l'extension.

Le fichier `.clang-format`, placé la racine du dépôt des TP, permet de spécifier
la mise en page des fichiers sources (c-files et h-files). VS-Code est configuré
pour que la mise en page ne s'effectue pas automatiquement. Pour l'activer, il 
faut utiliser la combinaison `command shift P` et entrer la command 
`Format Document`. Pour plus de détails, consulter la documentation en ligne
(http://clang.llvm.org/docs/ClangFormatStyleOptions.html)


## Sauvegarde des sources dans le dépôt Git

Sauvegarder les modifications dans le dépôt local

* Ouvrir un terminal (une shell Linux)
* Consulter l’état du dépôt
  ```
  $ git status
  ```
* Ajouter éventuellement les nouveaux fichiers
  ```
  $ git add *
  ```
* Commiter les modifications
  ```
  $ git commit -a -m "un commentaire..."
  ```

Synchroniser le dépôt local avec les dépôts centralisés (serveurs)

* Synchroniser avec le dépôt du cours
  ```
  $ git pull upstream master
  ```
* Synchroniser avec le dépôt personnel
  ```
  $ git pull origin master
  ```

Sauvegarder le résultat du travail dans le dépôt centralisé (serveur)

* Pousser la branche sur le dépôt
  ```
  $ git push origin master
  ```


## Travail à réaliser

* Compilez l'application sous `tp.01` à l’aide de l’environnement de développement
* Exécutez l'application en mode "pas à pas" en l’ayant chargé préalablement sur la cible
* Adaptez le code
    * 1er étudiant : modifiez le code afin d'afficher le message "HEIA-Fr" au lieu de "HELLO"
    * 2ème étudiant : modifiez le code afin de ralentir la vitesse d'affichage du message
* Mettre ensemble les 2 modifications. Si nécessaire utilisez l'utilitaire "`git mergetool`".
* Répondez aux questions
* Rédigez votre journal de laboratoire
* Rendez votre code et votre journal au travers de Git


## Impératifs

* Le travail pratique doit être exécuté par chaque étudiant
* Le dépôt Git d'un groupe doit contenir le tag de chacun des membres du groupe
* A la fin du TP, le workspace sur la machine locale de chacun des membres du groupe doit
  être synchronisé avec le dépôt Git commun du groupe


## Pêchés capitaux

Chaque groupe se fera un point d'honneur à éviter les 7 pêchés capitaux suivant

1. L'Orgueil - livrer un programme sans vérification
2. l'Avarice - écrire un code monolithique
3. L'Envie - utiliser des nombres magiques
4. La Colère - laisser du code mal indenté
5. L'Inconsistance - choisir des noms inappropriés
6. La Paresse - écrire un code sans commentaire
7. La Gourmandise - copier du code sans citation


## Questions...

* Quelle est la taille de chacune des variables ?
* Quelle est la taille du code?
* Comment procéder pour obtenir ces tailles ?
* Où se trouve chaque variable en mémoire (adresse absolue) ?
* Où se trouve le code en mémoire ?
* Est-il possible d’améliorer / d'optimiser le code ? Si oui, comment ?
* Comment fonctionne de la macro `ARRAY_SIZE(x)`


## Journal

* Le travail effectué durant le laboratoire devra être résumé et synthétisé dans un journal
  de laboratoire de 1 à 2 pages
    * En-tête
        * Etablissement: HEIA-FR (logo), institut, ...
        * Titre: Systèmes Embarqués I, journal, sujet (TP.01: Introduction)
        * Auteurs (nom, email, classe, ...)
        * Lieu et date
    * Heures de travail en dehors des heures de classe pour ce TP
    * Synthèse de l'étudiant sur ce qu'il a appris/exercé durant le TP
        * Non acquis
        * Acquis, mais à exercer encore
        * Parfaitement acquis
    * Réponses aux questions
    * Remarques / choses à retenir
    * Feedback sur le TP

* Remarque
    * Le journal doit être rendu sous le format PDF.
    * Il peut être rédigé en français, allemand ou anglais.
    * Il doit être stocké dans le dépôt Git avec le code source sous
        * *sources: .../tp/tp.01*
        * *journal: .../tp/tp.01/doc/report.pdf*

* Délai
    * Le journal et le code doivent être rendus au plus tard 7 jours après 
      le TP au plus tard à 23h59
