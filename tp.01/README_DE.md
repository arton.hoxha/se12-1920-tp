# TP.02 : Einführung in die modulare Programmierung in C 

## Ziele

Am Schluss des Labors sind die Studenten/Studentinnen fähig,

* die Entwicklungsprozesse im Microcomputing beschreiben können
* die Regeln der Laborarbeit anzuwenden
* die Geräte und Dokumentationen im Labor zu finden
* die Rolle des Compilers, Assemblers und Linkers beschreiben können
* den Beaglebone Black richtig zu handhaben und anzuschliessen
* die Grundfunktionen der Entwicklungsumgebung zu manipulieren
* die Hauptmerkmale des Mikroprozessors zu beschreiben
* den Fortschritt eines einfachen Programms zu analysieren

## Fragen...

* Wie gross ist die Größe der einzelnen Variablen?
* Wie gross ist der Code?
* Wie bekomme ich diese Grössen?
* Wo ist jede Variable im Speicher (absolute Adresse)?
* Wo ist der Code im Speicher?
* Ist es möglich, den Code zu verbessern / zu optimieren? Wenn ja, wie?
* Wie funktioniert das Makro `ARRAY_SIZE(x)`?
