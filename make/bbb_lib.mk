ARCH=$(addsuffix .a, $(addprefix lib, $(notdir $(CURDIR))))

mkfile_dir := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))
export mkfile_dir
include $(mkfile_dir)/bbb_tools.mk

.PHONY: all clean

all: $(OBJDIR)/ $(EXTRA_ALL) $(ARCH) 

clean: $(EXTRA_CLEAN)
	-$(DEL) $(ARCH) *~ $(EXTRAS)
	-$(RMDIR) .obj
	
$(ARCH): $(OBJS) 
	$(AR) cr $@ $(OBJS)

$(OBJDIR)/:
	-$(MKDIR) $(OBJDIR)

-include $(OBJS:.o=.d)

