ifeq ($(OS),Windows_NT)
DIRSEP=\\
SEP=&
LIBGCCPATH="c:/program files (x86)/gnu tools arm embedded/8 2019-q3-update/bin/../lib/gcc/arm-none-eabi/8.3.1"
DEL=del /F /S /Q
MKDIR=mkdir
RMDIR=rmdir /S /Q
else
DIRSEP=/
SEP=;
LIBGCCPATH=$(dir $(shell $(CC) -print-libgcc-file-name))
DEL=rm -Rf
MKDIR=mkdir -p
RMDIR=rm -Rf
endif

ifeq ($(TARGET),HOST)
	CC=gcc
	LD=gcc
	CFLAGS+=-Werror -Wall -Wextra -g -c -O0 -MD -std=gnu11
	CFLAGS+=$(EXTRA_CFLAGS)
	OBJDIR=.obj$(DIRSEP)host
	EXE=$(EXEC)_h
else
	TOOLCHAIN=arm-none-eabi-
	CC=$(TOOLCHAIN)gcc
	AS=$(TOOLCHAIN)as
	LD=$(TOOLCHAIN)ld
	AR=$(TOOLCHAIN)ar
	UFLAGS+=-mlittle-endian -funsigned-char -mno-unaligned-access
	UFLAGS+=-mcpu=cortex-a8 -mtune=cortex-a8 -msoft-float
	LFLAGS+=$(addprefix -I./, $(EXTRADIRS)) $(addprefix -I./, $(LIBDIRS))
	CFLAGS+=-Werror -Wall -Wextra -g -c -O0 -MD -std=gnu11
	CFLAGS+=$(EXTRA_CFLAGS) $(LFLAGS) $(UFLAGS)
	AFLAGS+=-mcpu=cortex-a8 -g
	CPPFLAGS+=-Werror -Wall -Wextra -g -c -O0 -MD $(LFLAGS) $(UFLAGS)
	CPPFLAGS+=-fno-rtti
	#CPPFLAGS+=-fno-threadsafe-statics
	CPPFLAGS+=-fno-exceptions -fnothrow-opt
	CPPFLAGS+=-std=gnu++14

	LDFLAGS+=--start-group -lbbb -lc -lgcc -lm  -lstdc++ --whole-archive $(addprefix -l, $(LIBDIRS)) --end-group
	LDFLAGS+= $(addprefix -L./, $(LIBDIRS))
	LDFLAGS+=--gc-sections --no-eh-frame-hdr
	LDFLAGS+=-L$(LIBGCCPATH)
	LDFLAGS+=-L"$(shell $(CC) -print-sysroot)"/lib
	ifeq ($(PROCESS),)
		EXE=$(EXEC)_a
		LDFLAGS+=-T $(mkfile_dir)/bbb.lds -Map=$(EXE).map
	else
		EXE=$(EXEC)_p
		LDFLAGS+=-T $(mkfile_dir)/bbb_process.lds -Map $(EXE).map
	endif

	OBJDIR=.obj$(DIRSEP)bbb
	ssrc = $(filter %.s, $(SRCS))
	Ssrc = $(filter %.S, $(SRCS))
	csrc = $(filter %.c, $(SRCS))
	OBJS+=$(addprefix $(OBJDIR)/, $(ASRC:.S=.o) $(Ssrc:.S=.o) $(ssrc:.s=.o) $(csrc:.c=.o) $(SCPP:.cpp=.o))
	EXTRA_LIBS+=$(foreach dir,$(LIBDIRS), $(addprefix $(dir)/, $(addsuffix .a, $(addprefix lib, $(dir)))))
endif

$(OBJDIR)/%.o: %.c
	$(CC) $(CFLAGS) $< -o $@

$(OBJDIR)/%.o: %.cpp
	$(CC) $<  $(CPPFLAGS) -o $@

$(OBJDIR)/%.o: %.S
	$(CC) $(CFLAGS) $< -o $@

$(OBJDIR)/%.o: %.s
	$(CC) $(CFLAGS) $< -o $@
